$(() => {
  $('#btn-cadastrar-novo').click(mostrarModalCadastrar)
  $('#btn-salvar-filme').click(btnSalvarClick)
  $('#modalEditarItem').on('hidden.bs.modal', limparCamposModalEditar)

  $('#link-alterar-foto').click(function (e) {
    e.preventDefault()
    $('[name="alterar-foto"]').click()
  })

  $('[name="alterar-foto"]').change(function () {
    const [foto] = this.files
    if (foto.size > 2097152) {
      alert('O arquivo deve ser menor que 2MB')
    } else {
      carregarImagem(foto, function () {
        $('.alert').html('Aguarde...')
      }, function (img) {
        $('[name="poster"]').attr('src', img)
      })
    }
  })

  renderItems()

  function renderItems() {
    const $tbody = $('table tbody')

    $tbody.empty()

    const linhas = database.obterTodos().map(filme => {
      const $tr = $('<tr />', {
        id: filme.id
      })

      const $tdNome = $('<td />', {
        html: filme.titulo
      })

      const $tdGenero = $('<td />', {
        html: filme.genero
      })

      const $tdImg = $('<td />', {
        html: $('<img>', {
          src: filme.poster,
          height: 80
        })
      })

      const $tdAcao = $('<td />', {
        html: montarCelulaAcao(filme.id)
      })

      $tr
        .append(
          $tdImg,
          $tdNome,
          $tdGenero,
          $tdAcao
        )
      return $tr
    })

    $tbody.append(linhas)
  }

  function montarCelulaAcao(id) {
    const $btnEditar = $('<button/>', {
      type: 'button',
      class: 'btn btn-sm btn-secondary',
      html: 'Editar'
    })

    const $btnVisualizar = $('<button/>', {
      type: 'button',
      class: 'btn btn-sm btn-info',
      html: 'Visualizar'
    })

    $btnEditar
      .click(function (e) {
        e.preventDefault()
        mostrarModalEditar(id)
      })

    $btnVisualizar
      .click(function (e) {
        e.preventDefault()
        mostrarModalVisualizar(id)
      })

    return $('<div />').append($btnVisualizar, ' ', $btnEditar)
  }

  function mostrarModalEditar(id) {
    const $modal = $('#modalEditarItem')
    const filme = database.obter(id)

    $modal.find('.modal-title').html('Editar filme')

    if (filme.poster) {
      $modal.find('[name="poster"]').attr('src', filme.poster)
    }

    $modal.find('[name="id"]').val(filme.id)
    $modal.find('[name="titulo"]').val(filme.titulo)
    $modal.find('[name="lancamento"]').val(filme.lancamento)
    $modal.find('[name="duracao"]').val(filme.duracao)
    $modal.find('[name="elenco"]').val(filme.elenco)
    $modal.find('[name="diretor"]').val(filme.diretor)
    $modal.find('[name="genero"]').val(filme.genero)
    $modal.find('[name="sinopse"]').val(filme.sinopse)

    $modal.find('#btn-salvar-filme').data('id', filme.id)

    $modal.modal('show')
  }

  function mostrarModalCadastrar() {
    const $modal = $('#modalEditarItem')
    $modal.find('.modal-title').html('Cadastrar novo filme')
    $modal.find('[name="id"]').parent().hide()
    $modal.modal('show')
  }

  function btnSalvarClick() {
    const id = $('#btn-salvar-filme').data('id')
    const item = database.obter(id)

    $modal = $('#modalEditarItem')

    const filme = {
      titulo: $modal.find('[name="titulo"]').val(),
      lancamento: $modal.find('[name="lancamento"]').val(),
      duracao: $modal.find('[name="duracao"]').val(),
      elenco: $modal.find('[name="elenco"]').val(),
      diretor: $modal.find('[name="diretor"]').val(),
      poster: $modal.find('[name="poster"]').attr('src'),
      genero: $modal.find('[name="genero"]').val(),
      sinopse: $modal.find('[name="sinopse"]').val(),
    }

    if (!filme.titulo) {
      alert('Informe pelo menos o título do filme')
      $modal.find('[name="titulo"]').prop('valid', false)
      return
    }

    if (item) {
      database.atualizar(id, filme)
      alert(`${filme.titulo} atualizado com sucesso!`)
    } else {
      database.adicionar(filme)
      alert(`${filme.titulo} foi cadastrado com sucesso!`)
    }

    $modal.modal('hide')
    renderItems()
  }

  function mostrarModalVisualizar(id) {
    const item = database.obter(id)
    const $modal = $('#modalVisualizarItem')

    if (item.poster) {
      $modal.find('.poster').attr('src', item.poster)
    }

    $modal.find('.id').html(item.id)
    $modal.find('.titulo').html(item.titulo)
    $modal.find('.lancamento').html(item.lancamento)
    $modal.find('.duracao').html(item.duracao)
    $modal.find('.elenco').html(item.elenco)
    $modal.find('.diretor').html(item.diretor)
    $modal.find('.genero').html(item.genero)
    $modal.find('.sinopse').html(item.sinopse)

    $modal.modal('show')
  }

  function limparCamposModalEditar() {
    $modal = $('#modalEditarItem')
    $modal.find('input').val('')
    $modal.find('textarea').val('')
    $modal.find('[name="poster"]').attr('src', 'img/sem-foto.png')
    $modal.find('[name="id"]').parent().show()
    $modal.find('#btn-salvar-filme').data('id', null)
  }

  function carregarImagem(file, start, done) {
    const fileReader = new FileReader()
    fileReader.readAsDataURL(file)
    fileReader.onloadstart = function (e) {
      start()
    }
    fileReader.onloadend = function (e) {
      done(this.result)
    }
  }
})