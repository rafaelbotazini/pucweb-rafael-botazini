// #region Fake DB
var fakeDb = [
  {
    "id": 1270797,
    "titulo": "Venom",
    "ano": 2018,
    "classificacao": "N/D",
    "lancamento": "05 de outubro de 2018",
    "duracao": "112 min",
    "genero": "Ação, Ficção Científica",
    "diretor": "Ruben Fleischer",
    "roteirista": "Jeff Pinkner (screenplay by), Scott Rosenberg (screenplay by), Kelly Marcel (screenplay by), Jeff Pinkner (screen story by), Scott Rosenberg (screen story by), Todd McFarlane (Marvel's Venom Character created by), David Michelinie (Marvel's Venom Character created by)",
    "elenco": "Tom Hardy, Michelle Williams, Riz Ahmed, Scott Haze",
    "sinopse": "Quando Eddie Brock adquire os poderes de um simbionte, ele terá que liberar seu alter-ego \"Venom\" para salvar sua vida.",
    "idioma": "Inglês",
    "pais": "EUA, China",
    "premiacoes": "N/D",
    "poster": "https://m.media-amazon.com/images/M/MV5BNzAwNzUzNjY4MV5BMl5BanBnXkFtZTgwMTQ5MzM0NjM@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.0/10"
      },
      {
        "Source": "Metacritic",
        "Value": "35/100"
      }
    ],
    "Metascore": 35,
    "imdbRating": 7.0,
    "imdbVotes": 114862,
    "Type": "movie",
    "DVD": "18 Jun 2013",
    "BoxOffice": "N/D",
    "Production": "Vis",
    "Website": "N/D",
    "Response": "True"
  },
  {
    "id": 4154756,
    "titulo": "Avengers: Infinity War",
    "ano": 2018,
    "classificacao": "PG-13",
    "lancamento": "27 de abril de 2018",
    "duracao": "149 min",
    "genero": "Ação, Aventura, Fantasia, Ficção Científica",
    "diretor": "Anthony Russo, Joe Russo",
    "roteirista": "Christopher Markus (screenplay by), Stephen McFeely (screenplay by), Stan Lee (based on the Marvel comics by), Jack Kirby (based on the Marvel comics by), Joe Simon (Captain America created by), Jack Kirby (Captain America created by), Steve Englehart (Star-Lord created by), Steve Gan (Star-Lord created by), Bill Mantlo (Rocket Raccoon created by), Keith Giffen (Rocket Raccoon created by), Jim Starlin (Thanos, Gamora and Drax created by), Stan Lee (Groot created by), Larry Lieber (Groot created by), Jack Kirby (Groot created by), Steve Englehart (Mantis created by), Don Heck (Mantis created by)",
    "elenco": "Robert Downey Jr., Chris Hemsworth, Mark Ruffalo, Chris Evans",
    "sinopse": "Os Vingadores e seus aliados devem estar dispostos a sacrificar tudo em uma tentativa de derrotar o poderoso Thanos antes que sua explosão de devastação e ruína ponha fim ao universo.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "N/D",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjMxNjY2MDU1OV5BMl5BanBnXkFtZTgwNzY1MTUwNTM@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "8.6/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "84%"
      },
      {
        "Source": "Metacritic",
        "Value": "68/100"
      }
    ],
    "Metascore": 68,
    "imdbRating": 8.6,
    "imdbVotes": 505383,
    "Type": "movie",
    "DVD": "14 Aug 2018",
    "BoxOffice": 664987816,
    "Production": "Walt Disney Pictures",
    "Website": "http://marvel.com/movies/movie/223/avengers_infinity_war",
    "Response": "True"
  },
  {
    "id": 1477834,
    "titulo": "Aquaman",
    "ano": 2018,
    "classificacao": "N/D",
    "lancamento": "21 de dezembro de 2018",
    "duracao": "N/D",
    "genero": "Ação, Aventura, Fantasia, Ficção Científica",
    "diretor": "James Wan",
    "roteirista": "Paul Norris (character), Mort Weisinger (character), Geoff Johns (story), James Wan (story), Will Beall (story), David Leslie Johnson-McGoldrick (screenplay), Will Beall (screenplay)",
    "elenco": "Amber Heard, Jason Momoa, Nicole Kidman, Dolph Lundgren",
    "sinopse": "Arthur Curry descobre que ele é o herdeiro do reino subaquático da Atlântida e deve dar um passo adiante para liderar seu povo e ser um herói para o mundo.",
    "idioma": "Inglês",
    "pais": "Austrália, EUA",
    "premiacoes": "N/D",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTA1NDM2ODUxOTNeQTJeQWpwZ15BbWU4MDgxOTEyMDYz._V1_SX300.jpg",
    "Ratings": [],
    "Metascore": "N/D",
    "imdbRating": "N/D",
    "imdbVotes": "N/D",
    "Type": "movie",
    "DVD": "N/D",
    "BoxOffice": "N/D",
    "Production": "Warner Bros. Pictures",
    "Website": "http://www.aquamanmovie.com/",
    "Response": "True"
  },
  {
    "id": 5095030,
    "titulo": "Ant-Man and the Wasp",
    "ano": 2018,
    "classificacao": "PG-13",
    "lancamento": "06 de julho de 2018",
    "duracao": "118 min",
    "genero": "Ação, Aventura, Comédia, Ficção Científica",
    "diretor": "Peyton Reed",
    "roteirista": "Chris McKenna, Erik Sommers, Paul Rudd, Andrew Barrer, Gabriel Ferrari, Stan Lee (based on the Marvel Comics by), Larry Lieber (based on the Marvel Comics by), Jack Kirby (based on the Marvel Comics by)",
    "elenco": "Paul Rudd, Evangeline Lilly, Michael Peña, Walton Goggins",
    "sinopse": "No rescaldo de 'Capitão América: Guerra Civil', Scott Lang enfrenta as conseqüências de suas escolhas tanto como super-herói quanto como pai. Enquanto ele luta para reequilibrar sua vida em casa com suas responsabilidades como Homem-Formiga, ele é confrontado por Hope van Dyne e Dr. Hank Pym com uma nova missão urgente. Scott deve mais uma vez vestir o terno e aprender a lutar ao lado de The Wasp enquanto a equipe trabalha em conjunto para descobrir os segredos de seu passado.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "N/D",
    "poster": "https://m.media-amazon.com/images/M/MV5BYjcyYTk0N2YtMzc4ZC00Y2E0LWFkNDgtNjE1MzZmMGE1YjY1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.2/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "88%"
      },
      {
        "Source": "Metacritic",
        "Value": "70/100"
      }
    ],
    "Metascore": 70,
    "imdbRating": 7.2,
    "imdbVotes": 140723,
    "Type": "movie",
    "DVD": "02 Oct 2018",
    "BoxOffice": "N/D",
    "Production": "Walt Disney Pictures",
    "Website": "https://marvel.com/antman",
    "Response": "True"
  },
  {
    "id": 5463162,
    "titulo": "Deadpool 2",
    "ano": 2018,
    "classificacao": "R",
    "lancamento": "18 de maio de 2018",
    "duracao": "119 min",
    "genero": "Ação, Aventura, Comédia, Ficção Científica",
    "diretor": "David Leitch",
    "roteirista": "Rhett Reese, Paul Wernick, Ryan Reynolds",
    "elenco": "Ryan Reynolds, Josh Brolin, Morena Baccarin, Julian Dennison",
    "sinopse": "O mercenário mutante Foul-mouthed Wade Wilson (AKA. Deadpool), reúne uma equipe de colegas mutantes para proteger um menino com habilidades sobrenaturais do cyborg brutal e viajante do tempo, Cable.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "N/D",
    "poster": "https://m.media-amazon.com/images/M/MV5BNjk1Njk3YjctMmMyYS00Y2I4LThhMzktN2U0MTMyZTFlYWQ5XkEyXkFqcGdeQXVyODM2ODEzMDA@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.8/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "82%"
      },
      {
        "Source": "Metacritic",
        "Value": "66/100"
      }
    ],
    "Metascore": 66,
    "imdbRating": 7.8,
    "imdbVotes": 294178,
    "Type": "movie",
    "DVD": "21 Aug 2018",
    "BoxOffice": "N/D",
    "Production": "20th Century Fox",
    "Website": "https://www.foxmovies.com/movies/deadpool-2",
    "Response": "True"
  },
  {
    "id": 4154664,
    "titulo": "Captain Marvel",
    "ano": 2019,
    "classificacao": "N/D",
    "lancamento": "08 de março de 2019",
    "duracao": "N/D",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Anna Boden, Ryan Fleck",
    "roteirista": "Anna Boden (screenplay by), Gene Colan (based on the Marvel comics by), Liz Flahive (screenplay by), Ryan Fleck (screenplay by), Meg LeFauve (screenplay by), Carly Mensch (screenplay by), Nicole Perlman (screenplay by), Geneva Robertson-Dworet (screenplay by), Roy Thomas (based on the Marvel comics by)",
    "elenco": "Brie Larson, Gemma Chan, Mckenna Grace, Chuku Modu",
    "sinopse": "Carol Danvers se torna um dos heróis mais poderosos do universo quando a Terra é capturada no meio de uma guerra galáctica entre duas raças alienígenas.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "N/D",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTYzNDc5NzY5OF5BMl5BanBnXkFtZTgwMjA0OTUzNjM@._V1_SX300.jpg",
    "Ratings": [],
    "Metascore": "N/D",
    "imdbRating": "N/D",
    "imdbVotes": "N/D",
    "Type": "movie",
    "DVD": "N/D",
    "BoxOffice": "N/D",
    "Production": "Walt Disney Pictures",
    "Website": "https://www.facebook.com/CaptainMarvelOfficial/",
    "Response": "True"
  },
  {
    "id": 1825683,
    "titulo": "Black Panther",
    "ano": 2018,
    "classificacao": "PG-13",
    "lancamento": "16 de fevereiro de 2018",
    "duracao": "134 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Ryan Coogler",
    "roteirista": "Ryan Coogler, Joe Robert Cole, Stan Lee (based on the Marvel comics by), Jack Kirby (based on the Marvel Comics by)",
    "elenco": "Chadwick Boseman, Michael B. Jordan, Lupita Nyong'o, Danai Gurira",
    "sinopse": "Após os eventos do Capitão América: Guerra Civil, o rei T'Challa retorna para casa, na nação africana de Wakanda, reclusa e tecnologicamente avançada, para servir como novo líder de seu país. No entanto, T'Challa logo descobre que ele é desafiado para o trono de facções dentro de seu próprio país. Quando dois inimigos conspiram para destruir Wakanda, o herói conhecido como Pantera Negra deve se unir ao agente da CIA Everett K. Ross e membros das forças especiais de Wakanadan, Dora Milaje, para impedir que Wakanda seja arrastado para uma guerra mundial.",
    "idioma": "Swahili, Nama, Inglês, Xhosa, coreano",
    "pais": "EUA, África do Sul, Coreia do Sul, Austrália",
    "premiacoes": "14 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTg1MTY2MjYzNV5BMl5BanBnXkFtZTgwMTc4NTMwNDI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.4/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "97%"
      },
      {
        "Source": "Metacritic",
        "Value": "88/100"
      }
    ],
    "Metascore": 88,
    "imdbRating": 7.4,
    "imdbVotes": 401608,
    "Type": "movie",
    "DVD": "15 May 2018",
    "BoxOffice": 501105037,
    "Production": "Marvel Studios",
    "Website": "https://www.facebook.com/BlackPantherMovie/",
    "Response": "True"
  },
  {
    "id": 3501632,
    "titulo": "Thor: Ragnarok",
    "ano": 2017,
    "classificacao": "PG-13",
    "lancamento": "03 de nov de 2017",
    "duracao": "130 min",
    "genero": "Ação, Aventura, Comédia, Fantasia, Ficção Científica",
    "diretor": "Taika Waititi",
    "roteirista": "Eric Pearson, Craig Kyle, Christopher L. Yost, Stan Lee (based on the Marvel comics by), Larry Lieber (based on the Marvel comics by), Jack Kirby (based on the Marvel comics by)",
    "elenco": "Chris Hemsworth, Tom Hiddleston, Cate Blanchett, Idris Elba",
    "sinopse": "Thor está preso no outro lado do universo e se encontra em uma corrida contra o tempo para voltar a Asgard para deter Ragnarok, a destruição de sua terra natal e o fim da civilização Asgardiana, nas mãos de uma nova ameaça toda poderosa. a implacável Hela.",
    "idioma": "Inglês",
    "pais": "EUA, Austrália",
    "premiacoes": "5 vitórias e 30 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjMyNDkzMzI1OF5BMl5BanBnXkFtZTgwODcxODg5MjI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.9/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "92%"
      },
      {
        "Source": "Metacritic",
        "Value": "74/100"
      }
    ],
    "Metascore": 74,
    "imdbRating": 7.9,
    "imdbVotes": 399094,
    "Type": "movie",
    "DVD": "06 Mar 2018",
    "BoxOffice": 314971245,
    "Production": "Walt Disney Pictures",
    "Website": "http://movies.disney.com/thor-ragnarok",
    "Response": "True"
  },
  {
    "id": 1386697,
    "titulo": "Suicide Squad",
    "ano": 2016,
    "classificacao": "PG-13",
    "lancamento": "05 de agosto de 2016",
    "duracao": "123 min",
    "genero": "Ação, Aventura, Fantasia, Ficção Científica",
    "diretor": "David Ayer",
    "roteirista": "David Ayer",
    "elenco": "Will Smith, Jaime FitzSimons, Ike Barinholtz, Margot Robbie",
    "sinopse": "É bom ser mau ... Reúna uma equipe dos super vilões mais perigosos do mundo, entregue-lhes o arsenal mais poderoso à disposição do governo e envie-os em uma missão para derrotar uma entidade enigmática e insuperável. A oficial de inteligência americana, Amanda Waller, determinou que apenas um grupo secretamente convocado de indivíduos desprezíveis e desprezíveis, com quase nada a perder, fará. No entanto, uma vez que eles percebem que não foram escolhidos para ter sucesso, mas escolhidos por sua culpabilidade patente quando inevitavelmente falharem, o Esquadrão Suicida resolverá morrer tentando, ou decidir que é cada um por si?",
    "idioma": "Inglês, japonês, espanhol",
    "pais": "EUA",
    "premiacoes": "Ganhou 1 Oscar. Mais 16 vitórias e 37 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjM1OTMxNzUyM15BMl5BanBnXkFtZTgwNjYzMTIzOTE@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.1/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "27%"
      },
      {
        "Source": "Metacritic",
        "Value": "40/100"
      }
    ],
    "Metascore": 40,
    "imdbRating": 6.1,
    "imdbVotes": 499787,
    "Type": "movie",
    "DVD": "13 Dec 2016",
    "BoxOffice": 325021779,
    "Production": "Warner Bros. Pictures",
    "Website": "http://www.suicidesquad.com/",
    "Response": "True"
  },
  {
    "id": 468569,
    "titulo": "The Dark Knight",
    "ano": 2008,
    "classificacao": "PG-13",
    "lancamento": "18 de julho de 2008",
    "duracao": "152 min",
    "genero": "Ação, Crime, Drama, Thriller",
    "diretor": "Christopher Nolan",
    "roteirista": "Jonathan Nolan (screenplay), Christopher Nolan (screenplay), Christopher Nolan (story), David S. Goyer (story), Bob Kane (characters)",
    "elenco": "Christian Bale, Heath Ledger, Aaron Eckhart, Michael Caine",
    "sinopse": "Situado dentro de um ano após os eventos de Batman Begins, Batman, o tenente James Gordon e o novo procurador Harvey Dent começarão a contornar os criminosos que afligem Gotham City até que um misterioso e sádico criminoso conhecido apenas quando o Coringa aparece em Gotham, criando uma nova onda de caos. A luta de Batman contra o Coringa torna-se profundamente pessoal, forçando-o a \"enfrentar tudo o que ele acredita\" e melhorar sua tecnologia para detê-lo. Um triângulo amoroso se desenvolve entre Bruce Wayne, Dent e Rachel Dawes.",
    "idioma": "Inglês, mandarim",
    "pais": "EUA, Reino Unido",
    "premiacoes": "Ganhou 2 Oscars. Outras 152 vitórias e 155 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "9.0/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "94%"
      },
      {
        "Source": "Metacritic",
        "Value": "84/100"
      }
    ],
    "Metascore": 84,
    "imdbRating": 9.0,
    "imdbVotes": 1980301,
    "Type": "movie",
    "DVD": "09 Dec 2008",
    "BoxOffice": 533316061,
    "Production": "Warner Bros. Pictures/Legendary",
    "Website": "http://thedarkknight.warnerbros.com/",
    "Response": "True"
  },
  {
    "id": 6565702,
    "titulo": "X-Men: Dark Phoenix",
    "ano": 2019,
    "classificacao": "N/D",
    "lancamento": "07 de junho de 2019",
    "duracao": "N/D",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Simon Kinberg",
    "roteirista": "John Byrne (story \"The Dark Phoenix Saga\"), Chris Claremont (story \"The Dark Phoenix Saga\"), Dave Cockrum (story \"The Dark Phoenix Saga\"), Simon Kinberg, Jack Kirby (comic book created by), Stan Lee (comic book created by)",
    "elenco": "Sophie Turner, Evan Peters, Jennifer Lawrence, Jessica Chastain",
    "sinopse": "Jean Grey começa a desenvolver incríveis poderes que a corrompem e a transformam em uma Fênix Negra. Agora os X-Men terão que decidir se a vida de um membro da equipe vale mais do que todas as pessoas que vivem no mundo.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "N/D",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjM0NjcyMzU5Nl5BMl5BanBnXkFtZTgwNTE2MzU0NjM@._V1_SX300.jpg",
    "Ratings": [],
    "Metascore": "N/D",
    "imdbRating": "N/D",
    "imdbVotes": "N/D",
    "Type": "movie",
    "DVD": "N/D",
    "BoxOffice": "N/D",
    "Production": "20th Century Fox",
    "Website": "http://DarkPhoenix.com",
    "Response": "True"
  },
  {
    "id": 974015,
    "titulo": "Justice League",
    "ano": 2017,
    "classificacao": "PG-13",
    "lancamento": "17 de nov de 2017",
    "duracao": "120 min",
    "genero": "Ação, Aventura, Fantasia, Ficção Científica",
    "diretor": "Zack Snyder",
    "roteirista": "Jerry Siegel (Superman created by), Joe Shuster (Superman created by), Chris Terrio (story by), Zack Snyder (story by), Chris Terrio (screenplay by), Joss Whedon (screenplay by), Gardner Fox (Justice League of America created by), Bob Kane (Batman created by), Bill Finger (Batman created by), William Moulton Marston (Wonder Woman created by), Jack Kirby (Fourth World created by)",
    "elenco": "Ben Affleck, Henry Cavill, Amy Adams, Gal Gadot",
    "sinopse": "Alimentado por sua fé restaurada na humanidade e inspirado pelo ato abnegado do Super-Homem, Bruce Wayne pede a ajuda de sua recém-encontrada aliada, Diana Prince, para enfrentar um inimigo ainda maior. Juntos, Batman e Wonder Woman trabalham rapidamente para encontrar e recrutar uma equipe de metahumanos para enfrentar essa ameaça recém-desperta. Mas, apesar da formação dessa inédita liga de heróis - Batman, Mulher Maravilha, Aquaman, Cyborg e The Flash - pode já ser tarde demais para salvar o planeta de um ataque de proporções catastróficas.",
    "idioma": "Inglês, irlandês, russo, islandês",
    "pais": "Reino Unido, Canadá, EUA",
    "premiacoes": "1 vitória e 6 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BYWVhZjZkYTItOGIwYS00NmRkLWJlYjctMWM0ZjFmMDU4ZjEzXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.6/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "40%"
      },
      {
        "Source": "Metacritic",
        "Value": "45/100"
      }
    ],
    "Metascore": 45,
    "imdbRating": 6.6,
    "imdbVotes": 288827,
    "Type": "movie",
    "DVD": "13 Mar 2018",
    "BoxOffice": 227032490,
    "Production": "Warner Bros. Pictures",
    "Website": "http://www.justiceleaguethemovie.com/",
    "Response": "True"
  },
  {
    "id": 451279,
    "titulo": "Wonder Woman",
    "ano": 2017,
    "classificacao": "PG-13",
    "lancamento": "2 de junho de 2017",
    "duracao": "141 min",
    "genero": "Ação, Aventura, Fantasia, Ficção Científica, Guerra",
    "diretor": "Patty Jenkins",
    "roteirista": "Allan Heinberg (screenplay by), Zack Snyder (story by), Allan Heinberg (story by), Jason Fuchs (story by), William Moulton Marston (Wonder Woman created by)",
    "elenco": "Gal Gadot, Chris Pine, Connie Nielsen, Robin Wright",
    "sinopse": "Diana, princesa das amazonas, treinada para ser uma guerreira invencível. Criada em uma paradisíaca ilha paradisíaca, quando um piloto bate em suas costas e conta que um grande conflito está ocorrendo no mundo exterior, Diana a deixa em casa, convencida de que ela pode parar a ameaça. Lutando ao lado do homem em uma guerra para acabar com todas as guerras, Diana descobrirá seus poderes completos e seu verdadeiro destino.",
    "idioma": "Inglês, alemão, holandês, francês, espanhol, chinês, grego, antigo (até 1453), índio norte-americano",
    "pais": "EUA, China, Hong Kong",
    "premiacoes": "17 vitórias e 54 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNDFmZjgyMTEtYTk5MC00NmY0LWJhZjktOWY2MzI5YjkzODNlXkEyXkFqcGdeQXVyMDA4NzMyOA@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.5/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "93%"
      },
      {
        "Source": "Metacritic",
        "Value": "76/100"
      }
    ],
    "Metascore": 76,
    "imdbRating": 7.5,
    "imdbVotes": 444037,
    "Type": "movie",
    "DVD": "19 Sep 2017",
    "BoxOffice": 412400625,
    "Production": "Warner Bros. Pictures",
    "Website": "http://wonderwomanfilm.com/",
    "Response": "True"
  },
  {
    "id": 2250912,
    "titulo": "Spider-Man: Homecoming",
    "ano": 2017,
    "classificacao": "PG-13",
    "lancamento": "07 de julho de 2017",
    "duracao": "133 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Jon Watts",
    "roteirista": "Jonathan Goldstein (screenplay by), John Francis Daley (screenplay by), Jon Watts (screenplay by), Christopher Ford (screenplay by), Chris McKenna (screenplay by), Erik Sommers (screenplay by), Jonathan Goldstein (screen story by), John Francis Daley (screen story by), Stan Lee (based on the Marvel comic book by), Steve Ditko (based on the Marvel comic book by), Joe Simon (Captain America created by), Jack Kirby (Captain America created by)",
    "elenco": "Tom Holland, Michael Keaton, Robert Downey Jr., Marisa Tomei",
    "sinopse": "Emocionado por sua experiência com os Vingadores, Peter volta para casa, onde mora com sua tia May, sob o olhar atento de seu novo mentor Tony Stark, Peter tenta voltar à sua rotina diária normal - distraído por pensamentos de provar ser mais do que apenas o seu simpático Homem-Aranha - mas quando o Vulture surge como um novo vilão, tudo o que Peter considera mais importante será ameaçado.",
    "idioma": "Inglês espanhol",
    "pais": "EUA",
    "premiacoes": "4 vitórias e 9 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNTk4ODQ1MzgzNl5BMl5BanBnXkFtZTgwMTMyMzM4MTI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.5/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "92%"
      },
      {
        "Source": "Metacritic",
        "Value": "73/100"
      }
    ],
    "Metascore": 73,
    "imdbRating": 7.5,
    "imdbVotes": 366978,
    "Type": "movie",
    "DVD": "17 Oct 2017",
    "BoxOffice": 334166825,
    "Production": "Sony Pictures",
    "Website": "http://www.sonypictures.com/movies/spidermanhomecoming/",
    "Response": "True"
  },
  {
    "id": 2015381,
    "titulo": "Guardians of the Galaxy",
    "ano": 2014,
    "classificacao": "PG-13",
    "lancamento": "01 de agosto de 2014",
    "duracao": "121 min",
    "genero": "Ação, Aventura, Comédia, Ficção Científica",
    "diretor": "James Gunn",
    "roteirista": "James Gunn, Nicole Perlman, Dan Abnett (based on the Marvel comics by), Andy Lanning (based on the Marvel comics by), Bill Mantlo (character created by: Rocket Raccoon), Keith Giffen (character created by: Rocket Raccoon), Jim Starlin (characters created by: Drax the Destroyer, Gamora & Thanos), Steve Englehart (character created by: Star-Lord), Steve Gan (character created by: Star-Lord), Steve Gerber (character created by: Howard the Duck), Val Mayerik (character created by: Howard the Duck)",
    "elenco": "Chris Pratt, Zoe Saldana, Dave Bautista, Vin Diesel",
    "sinopse": "Depois de roubar uma esfera misteriosa nos confins do espaço exterior, Peter Quill, da Terra, é agora o alvo principal de uma caçada liderada pelo vilão conhecido como Ronan, o Acusador. Para ajudar a combater Ronan e sua equipe e salvar a galáxia de seu poder, Quill cria uma equipe de heróis espaciais conhecidos como os \"Guardiões da Galáxia\" para salvar o mundo.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "Nomeado para 2 Oscars. Mais 52 vitórias e 99 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTAwMjU5OTgxNjZeQTJeQWpwZ15BbWU4MDUxNDYxODEx._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "8.1/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "91%"
      },
      {
        "Source": "Metacritic",
        "Value": "76/100"
      }
    ],
    "Metascore": 76,
    "imdbRating": 8.1,
    "imdbVotes": 879528,
    "Type": "movie",
    "DVD": "09 Dec 2014",
    "BoxOffice": 270592504,
    "Production": "Walt Disney Pictures",
    "Website": "http://marvel.com/guardians",
    "Response": "True"
  },
  {
    "id": 3385516,
    "titulo": "X-Men: Apocalypse",
    "ano": 2016,
    "classificacao": "PG-13",
    "lancamento": "27 de maio de 2016",
    "duracao": "144 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Bryan Singer",
    "roteirista": "Simon Kinberg (screenplay by), Bryan Singer (story by), Simon Kinberg (story by), Michael Dougherty (story by), Dan Harris (story by)",
    "elenco": "James McAvoy, Michael Fassbender, Jennifer Lawrence, Nicholas Hoult",
    "sinopse": "Desde o alvorecer da civilização, ele foi adorado como um deus. Apocalypse, o primeiro e mais poderoso mutante do universo X-Men da Marvel, acumulou os poderes de muitos outros mutantes, tornando-se imortal e invencível. Ao despertar depois de milhares de anos, ele está desiludido com o mundo como ele o encontra e recruta uma equipe de poderosos mutantes, incluindo um desanimado Magneto, para limpar a humanidade e criar uma nova ordem mundial, sobre a qual ele reinará. Como o destino da Terra está na balança, Raven com a ajuda do Professor X deve liderar uma equipe de jovens X-Men para impedir seu maior inimigo e salvar a humanidade da completa destruição.",
    "idioma": "Inglês, polonês, alemão, árabe, egípcio (antigo)",
    "pais": "EUA",
    "premiacoes": "1 vitória e 14 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjU1ODM1MzYxN15BMl5BanBnXkFtZTgwOTA4NDE2ODE@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.0/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "48%"
      },
      {
        "Source": "Metacritic",
        "Value": "52/100"
      }
    ],
    "Metascore": 52,
    "imdbRating": 7.0,
    "imdbVotes": 331377,
    "Type": "movie",
    "DVD": "04 Oct 2016",
    "BoxOffice": 135729385,
    "Production": "20th Century Fox",
    "Website": "https://www.facebook.com/xmenmovies",
    "Response": "True"
  },
  {
    "id": 1431045,
    "titulo": "Deadpool",
    "ano": 2016,
    "classificacao": "R",
    "lancamento": "12 de fevereiro de 2016",
    "duracao": "108 min",
    "genero": "Ação, Aventura, Comédia, Ficção Científica",
    "diretor": "Tim Miller",
    "roteirista": "Rhett Reese, Paul Wernick",
    "elenco": "Ryan Reynolds, Karan Soni, Ed Skrein, Michael Benyaer",
    "sinopse": "Esta é a história de origem do ex-agente das Forças Especiais que se tornou mercenário Wade Wilson, que depois de ter sido submetido a um experimento desonesto que o deixa com poderes curativos acelerados, adota o alter-ego Deadpool. Armado com suas novas habilidades e um senso de humor escuro e distorcido, Deadpool persegue o homem que quase destruiu sua vida.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "Nomeado por 2 Globos de Ouro. Mais 27 vitórias e 73 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BYzE5MjY1ZDgtMTkyNC00MTMyLThhMjAtZGI5OTE1NzFlZGJjXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "8.0/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "84%"
      },
      {
        "Source": "Metacritic",
        "Value": "65/100"
      }
    ],
    "Metascore": 65,
    "imdbRating": 8.0,
    "imdbVotes": 771816,
    "Type": "movie",
    "DVD": "10 May 2016",
    "BoxOffice": 328674489,
    "Production": "20th Century Fox",
    "Website": "http://www.foxmovies.com/deadpool",
    "Response": "True"
  },
  {
    "id": 2975590,
    "titulo": "Batman v Superman: Dawn of Justice",
    "ano": 2016,
    "classificacao": "PG-13",
    "lancamento": "25 de março de 2016",
    "duracao": "151 min",
    "genero": "Ação, Aventura, Fantasia, Ficção Científica",
    "diretor": "Zack Snyder",
    "roteirista": "Chris Terrio, David S. Goyer, Bob Kane (Batman created by), Bill Finger (Batman created by), Jerry Siegel (Superman created by), Joe Shuster (Superman created by), William Moulton Marston (character created by: Wonder Woman)",
    "elenco": "Ben Affleck, Henry Cavill, Amy Adams, Jesse Eisenberg",
    "sinopse": "O público em geral está preocupado em ter Superman em seu planeta e deixar o \"Cavaleiro das Trevas\" - Batman - perseguir as ruas de Gotham. Enquanto isto está acontecendo, um Batman com fobia de poder tenta atacar o Super-Homem. Enquanto isso, o Super-Homem tenta tomar uma decisão, e Lex Luthor, o mentor do crime e milionário, tenta usar suas próprias vantagens para combater o \"Homem de Aço\".",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "14 vitórias e 30 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BYThjYzcyYzItNTVjNy00NDk0LTgwMWQtYjMwNmNlNWJhMzMyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.5/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "27%"
      },
      {
        "Source": "Metacritic",
        "Value": "44/100"
      }
    ],
    "Metascore": 44,
    "imdbRating": 6.5,
    "imdbVotes": 550417,
    "Type": "movie",
    "DVD": "19 Jul 2016",
    "BoxOffice": 293792936,
    "Production": "Warner Bros. Pictures",
    "Website": "http://www.facebook.com/batmanvsuperman",
    "Response": "True"
  },
  {
    "id": 3896198,
    "titulo": "Guardians of the Galaxy Vol. 2",
    "ano": 2017,
    "classificacao": "PG-13",
    "lancamento": "05 de maio de 2017",
    "duracao": "136 min",
    "genero": "Ação, Aventura, Comédia, Ficção Científica",
    "diretor": "James Gunn",
    "roteirista": "James Gunn, Dan Abnett (based on the Marvel comics by), Andy Lanning (based on the Marvel comics by), Steve Englehart (Star-Lord created by), Steve Gan (Star-Lord created by), Jim Starlin (Gamora and Drax created by), Stan Lee (Groot created by), Larry Lieber (Groot created by), Jack Kirby (Groot created by), Bill Mantlo (Rocket Raccoon created by), Keith Giffen (Rocket Raccoon created by), Steve Gerber (Howard the Duck created by), Val Mayerik (Howard the Duck created by)",
    "elenco": "Chris Pratt, Zoe Saldana, Dave Bautista, Vin Diesel",
    "sinopse": "Depois de salvar Xandar da ira de Ronan, os Guardiões agora são reconhecidos como heróis. Agora, a equipe deve ajudar seu líder, o Star Lord (Chris Pratt) a descobrir a verdade por trás de sua verdadeira herança. Ao longo do caminho, velhos inimigos se voltam para aliados e a traição está florescendo. E os Guardiões acham que eles estão enfrentando uma nova ameaça devastadora que está fora para governar a galáxia.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "Nomeado para 1 Oscar. Mais 12 vitórias e 42 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTg2MzI1MTg3OF5BMl5BanBnXkFtZTgwNTU3NDA2MTI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.7/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "83%"
      },
      {
        "Source": "Metacritic",
        "Value": "67/100"
      }
    ],
    "Metascore": 67,
    "imdbRating": 7.7,
    "imdbVotes": 416247,
    "Type": "movie",
    "DVD": "22 Aug 2017",
    "BoxOffice": 389804217,
    "Production": "Walt Disney Pictures",
    "Website": "https://marvel.com/guardians",
    "Response": "True"
  },
  {
    "id": 1345836,
    "titulo": "The Dark Knight Rises",
    "ano": 2012,
    "classificacao": "PG-13",
    "lancamento": "20 de julho de 2012",
    "duracao": "164 min",
    "genero": "Thriller de açao",
    "diretor": "Christopher Nolan",
    "roteirista": "Jonathan Nolan (screenplay), Christopher Nolan (screenplay), Christopher Nolan (story), David S. Goyer (story), Bob Kane (characters)",
    "elenco": "Christian Bale, Gary Oldman, Tom Hardy, Joseph Gordon-Levitt",
    "sinopse": "Apesar de sua reputação manchada após os eventos de O Cavaleiro das Trevas, em que ele levou o rap para os crimes de Dent, Batman se sente obrigado a intervir para ajudar a cidade e sua força policial que está lutando para lidar com os planos de Bane para destruir a cidade.",
    "idioma": "Inglês, árabe",
    "pais": "Reino Unido, EUA",
    "premiacoes": "Indicado para 1 prêmio de filme BAFTA. Mais 38 vitórias e 102 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTk4ODQzNDY3Ml5BMl5BanBnXkFtZTcwODA0NTM4Nw@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "8.4/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "87%"
      },
      {
        "Source": "Metacritic",
        "Value": "78/100"
      }
    ],
    "Metascore": 78,
    "imdbRating": 8.4,
    "imdbVotes": 1338494,
    "Type": "movie",
    "DVD": "03 Dec 2012",
    "BoxOffice": 448130642,
    "Production": "Warner Bros. Pictures",
    "Website": "http://www.thedarkknightrises.com/",
    "Response": "True"
  },
  {
    "id": 478970,
    "titulo": "Ant-Man",
    "ano": 2015,
    "classificacao": "PG-13",
    "lancamento": "17 de julho de 2015",
    "duracao": "117 min",
    "genero": "Ação, Aventura, Comédia, Crime, Ficção científica",
    "diretor": "Peyton Reed",
    "roteirista": "Edgar Wright (screenplay by), Joe Cornish (screenplay by), Adam McKay (screenplay by), Paul Rudd (screenplay by), Edgar Wright (story by), Joe Cornish (story by), Stan Lee (based on the Marvel comics by), Larry Lieber (based on the Marvel comics by), Jack Kirby (based on the Marvel comics by)",
    "elenco": "Paul Rudd, Michael Douglas, Evangeline Lilly, Corey Stoll",
    "sinopse": "Armado com a surpreendente capacidade de diminuir em escala, mas aumentar em força, o conde Scott Lang deve abraçar seu herói interior e ajudar seu mentor, Dr. Hank Pym, a proteger o segredo por trás de seu espetacular traje de Homem-Formiga a partir de uma nova geração. ameaças imponentes. Contra obstáculos aparentemente intransponíveis, Pym e Lang devem planejar e realizar um assalto que salvará o mundo.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "Indicado para 1 prêmio de filme BAFTA. Mais 3 vitórias e 32 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjM2NTQ5Mzc2M15BMl5BanBnXkFtZTgwNTcxMDI2NTE@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.3/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "82%"
      },
      {
        "Source": "Metacritic",
        "Value": "64/100"
      }
    ],
    "Metascore": 64,
    "imdbRating": 7.3,
    "imdbVotes": 453797,
    "Type": "movie",
    "DVD": "08 Dec 2015",
    "BoxOffice": 138002223,
    "Production": "Disney/Marvel",
    "Website": "http://www.facebook.com/antman",
    "Response": "True"
  },
  {
    "id": 3315342,
    "titulo": "Logan",
    "ano": 2017,
    "classificacao": "R",
    "lancamento": "03 de março de 2017",
    "duracao": "137 min",
    "genero": "Ação, Drama, Ficção Científica, Suspense",
    "diretor": "James Mangold",
    "roteirista": "James Mangold (story by), Scott Frank (screenplay by), James Mangold (screenplay by), Michael Green (screenplay by)",
    "elenco": "Hugh Jackman, Patrick Stewart, Dafne Keen, Boyd Holbrook",
    "sinopse": "Em 2029, a população mutante encolheu significativamente e os X-Men se desfizeram. Logan, cujo poder de auto-cura está diminuindo, se entregou ao álcool e agora ganha a vida como motorista. Ele cuida do velho Professor X que ele mantém escondido. Um dia, uma mulher estranha pede a Logan para conduzir uma garota chamada Laura para a fronteira canadense. No começo ele se recusa, mas o professor espera há muito tempo que ela apareça. Laura possui uma extraordinária destreza de luta e é, em muitos aspectos, como Wolverine. Ela é perseguida por figuras sinistras que trabalham para uma corporação poderosa; isso é porque o DNA dela contém o segredo que a conecta ao Logan. Uma incansável busca começa - nesta terceira apresentação cinematográfica com o personagem Wolverine da revista em quadrinhos da Marvel, vemos os super-heróis afetados por problemas cotidianos. Eles estão envelhecendo, doentes e lutando para sobreviver financeiramente. Um decrépito Logan é forçado a se perguntar se ele pode ou mesmo quer colocar seus poderes remanescentes em bom uso. Parece que no futuro próximo, os tempos em que eles foram capazes de colocar o mundo a salvo com garras afiadas e poderes telepáticos estão agora acabados.",
    "idioma": "Inglês espanhol",
    "pais": "EUA",
    "premiacoes": "Nomeado para 1 Oscar. Mais 13 vitórias e 55 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BYzc5MTU4N2EtYTkyMi00NjdhLTg3NWEtMTY4OTEyMzJhZTAzXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "8.1/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "93%"
      },
      {
        "Source": "Metacritic",
        "Value": "77/100"
      }
    ],
    "Metascore": 77,
    "imdbRating": 8.1,
    "imdbVotes": 513917,
    "Type": "movie",
    "DVD": "23 May 2017",
    "BoxOffice": 226276809,
    "Production": "20th Century Fox",
    "Website": "http://www.foxmovies.com/movies/logan",
    "Response": "True"
  },
  {
    "id": 848228,
    "titulo": "The Avengers",
    "ano": 2012,
    "classificacao": "PG-13",
    "lancamento": "04 de maio de 2012",
    "duracao": "143 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Joss Whedon",
    "roteirista": "Joss Whedon (screenplay), Zak Penn (story), Joss Whedon (story)",
    "elenco": "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
    "sinopse": "Nick Fury é o diretor da SHIELD, uma agência internacional de manutenção da paz. A agência é quem é quem, da Marvel Super Heroes, com Iron Man, O Incrível Hulk, Thor, Capitão América, Hawkeye e Black Widow. Quando a segurança global é ameaçada por Loki e seus companheiros, Nick Fury e sua equipe precisarão de todos os seus poderes para salvar o mundo do desastre.",
    "idioma": "Inglês, russo, hindi",
    "pais": "EUA",
    "premiacoes": "Nomeado para 1 Oscar. Outras 38 vitórias e 79 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "8.1/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "92%"
      },
      {
        "Source": "Metacritic",
        "Value": "69/100"
      }
    ],
    "Metascore": 69,
    "imdbRating": 8.1,
    "imdbVotes": 1127047,
    "Type": "movie",
    "DVD": "25 Sep 2012",
    "BoxOffice": 623279547,
    "Production": "Walt Disney Pictures",
    "Website": "http://marvel.com/avengers_movie",
    "Response": "True"
  },
  {
    "id": 2395427,
    "titulo": "Avengers: Age of Ultron",
    "ano": 2015,
    "classificacao": "PG-13",
    "lancamento": "01 de maio de 2015",
    "duracao": "141 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Joss Whedon",
    "roteirista": "Joss Whedon, Stan Lee (based on the Marvel comics by), Jack Kirby (based on the Marvel comics by), Joe Simon (character created by: Captain America), Jack Kirby (character created by: Captain America), Jim Starlin (character created by: Thanos)",
    "elenco": "Robert Downey Jr., Chris Hemsworth, Mark Ruffalo, Chris Evans",
    "sinopse": "Tony Stark cria o Programa Ultron para proteger o mundo, mas quando o programa de manutenção da paz se torna hostil, os Vingadores entram em ação para tentar derrotar um inimigo praticamente impossível juntos. Os heróis mais poderosos da Terra devem se unir novamente para proteger o mundo da extinção global.",
    "idioma": "Inglês, coreano",
    "pais": "EUA",
    "premiacoes": "7 vitórias e 45 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTM4OGJmNWMtOTM4Ni00NTE3LTg3MDItZmQxYjc4N2JhNmUxXkEyXkFqcGdeQXVyNTgzMDMzMTg@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.4/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "74%"
      },
      {
        "Source": "Metacritic",
        "Value": "66/100"
      }
    ],
    "Metascore": 66,
    "imdbRating": 7.4,
    "imdbVotes": 608504,
    "Type": "movie",
    "DVD": "02 Oct 2015",
    "BoxOffice": 429113729,
    "Production": "Walt Disney Pictures",
    "Website": "http://marvel.com/avengers",
    "Response": "True"
  },
  {
    "id": 371746,
    "titulo": "Iron Man",
    "ano": 2008,
    "classificacao": "PG-13",
    "lancamento": "02 de maio de 2008",
    "duracao": "126 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Jon Favreau",
    "roteirista": "Mark Fergus (screenplay), Hawk Ostby (screenplay), Art Marcum (screenplay), Matt Holloway (screenplay), Stan Lee (characters), Don Heck (characters), Larry Lieber (characters), Jack Kirby (characters)",
    "elenco": "Robert Downey Jr., Terrence Howard, Jeff Bridges, Gwyneth Paltrow",
    "sinopse": "Tony Stark. Gênio, bilionário, playboy, filantropo. Filho do lendário inventor e contratado de armas Howard Stark. Quando Tony Stark é designado para dar uma apresentação de armas a uma unidade iraquiana liderada pelo tenente-coronel James Rhodes, ele deu uma volta nas linhas inimigas. Esse passeio termina mal quando o Humvee de Stark em que ele está andando é atacado por combatentes inimigos. Ele sobrevive - mal - com um baú cheio de estilhaços e uma bateria de carro ligada ao seu coração. Para sobreviver, ele cria uma maneira de miniaturizar a bateria e descobre que a bateria pode alimentar outra coisa. Assim o homem de ferro é nascido. Ele usa o dispositivo primitivo para escapar da caverna no Iraque. Uma vez de volta para casa, ele começa a trabalhar no aperfeiçoamento do traje Homem de Ferro. Mas o homem encarregado das Indústrias Stark tem planos próprios para assumir a tecnologia de Tony em outros assuntos.",
    "idioma": "Inglês, persa, urdu, árabe, húngaro",
    "pais": "EUA",
    "premiacoes": "Nomeado para 2 Oscars. Mais 20 vitórias e 65 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.9/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "93%"
      },
      {
        "Source": "Metacritic",
        "Value": "79/100"
      }
    ],
    "Metascore": 79,
    "imdbRating": 7.9,
    "imdbVotes": 812955,
    "Type": "movie",
    "DVD": "30 Sep 2008",
    "BoxOffice": 318298180,
    "Production": "Paramount Pictures",
    "Website": "http://www.ironmanmovie.com/",
    "Response": "True"
  },
  {
    "id": 1211837,
    "titulo": "Doctor Strange",
    "ano": 2016,
    "classificacao": "PG-13",
    "lancamento": "04 de novembro de 2016",
    "duracao": "115 min",
    "genero": "Ação, Aventura, Fantasia, Ficção Científica",
    "diretor": "Scott Derrickson",
    "roteirista": "Jon Spaihts, Scott Derrickson, C. Robert Cargill, Stan Lee (based on the Marvel comics by), Steve Ditko (based on the Marvel comics by)",
    "elenco": "Benedict Cumberbatch, Chiwetel Ejiofor, Rachel McAdams, Benedict Wong",
    "sinopse": "\"Doctor Strange\", da Marvel, segue a história do talentoso neurocirurgião Doutor Stephen Strange que, após um trágico acidente de carro, deve deixar o ego de lado e aprender os segredos de um mundo oculto de misticismo e dimensões alternativas. Baseado no Greenwich Village de Nova York, Doctor Strange deve atuar como um intermediário entre o mundo real e o que está além, utilizando uma vasta gama de habilidades e artefatos metafísicos para proteger o universo cinematográfico da Marvel.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "Nomeado para 1 Oscar. Mais 20 vitórias e 64 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNjgwNzAzNjk1Nl5BMl5BanBnXkFtZTgwMzQ2NjI1OTE@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.5/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "89%"
      },
      {
        "Source": "Metacritic",
        "Value": "72/100"
      }
    ],
    "Metascore": 72,
    "imdbRating": 7.5,
    "imdbVotes": 456670,
    "Type": "movie",
    "DVD": "28 Feb 2017",
    "BoxOffice": 232630718,
    "Production": "Walt Disney Pictures",
    "Website": "http://marvel.com/doctorstrange",
    "Response": "True"
  },
  {
    "id": 372784,
    "titulo": "Batman Begins",
    "ano": 2005,
    "classificacao": "PG-13",
    "lancamento": "15 de junho de 2005",
    "duracao": "140 min",
    "genero": "Ação, Aventura, Thriller",
    "diretor": "Christopher Nolan",
    "roteirista": "Bob Kane (characters), David S. Goyer (story), Christopher Nolan (screenplay), David S. Goyer (screenplay)",
    "elenco": "Christian Bale, Michael Caine, Liam Neeson, Katie Holmes",
    "sinopse": "Quando seus pais morrem, o playboy bilionário Bruce Wayne se muda para a Ásia, onde é orientado por Henri Ducard e Ra's Al Ghul em como combater o mal. Ao aprender sobre o plano para acabar com o mal em Gotham City por Bruce, Bruce evita que esse plano continue e volte para sua casa. De volta ao seu ambiente original, Bruce adota a imagem de um morcego para causar medo nos criminosos e nos corruptos como o ícone conhecido como \"Batman\". Mas não fica quieto por muito tempo.",
    "idioma": "Inglês, urdu, mandarim",
    "pais": "EUA, Reino Unido",
    "premiacoes": "Nomeado para 1 Oscar. Mais 14 vitórias e 72 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BZmUwNGU2ZmItMmRiNC00MjhlLTg5YWUtODMyNzkxODYzMmZlXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "8.3/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "84%"
      },
      {
        "Source": "Metacritic",
        "Value": "70/100"
      }
    ],
    "Metascore": 70,
    "imdbRating": 8.3,
    "imdbVotes": 1150889,
    "Type": "movie",
    "DVD": "18 Oct 2005",
    "BoxOffice": 204100000,
    "Production": "Warner Bros. Pictures",
    "Website": "http://www.batmanbegins.com/",
    "Response": "True"
  },
  {
    "id": 448115,
    "titulo": "Shazam!",
    "ano": 2019,
    "classificacao": "N/D",
    "lancamento": "05 de abril de 2019",
    "duracao": "N/D",
    "genero": "Ação, fantasia, ficção científica",
    "diretor": "David F. Sandberg",
    "roteirista": "Henry Gayden (screenplay by), Henry Gayden (story by), Darren Lemke (story by), Bill Parker (Shazam created by), C.C. Beck (Shazam created by)",
    "elenco": "Zachary Levi, Mark Strong, Michelle Borth, Djimon Hounsou",
    "sinopse": "O jovem Billy Batson encontra-se dotado do poder do mago Shazam para lutar contra as forças do mal. Com a sabedoria de Salomão, a força de Hércules, a resistência de Atlas, o poder de Zeus, a coragem de Aquiles e a velocidade de Mercúrio, ele é o detentor do poder de SHAZAM; ele é o Capitão Marvel.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "N/D",
    "poster": "https://m.media-amazon.com/images/M/MV5BNTkxNDU1NzMwOF5BMl5BanBnXkFtZTgwMjA5MDkwNTM@._V1_SX300.jpg",
    "Ratings": [],
    "Metascore": "N/D",
    "imdbRating": "N/D",
    "imdbVotes": "N/D",
    "Type": "movie",
    "DVD": "N/D",
    "BoxOffice": "N/D",
    "Production": "Warner Bros. Pictures",
    "Website": "https://www.facebook.com/ShazamMovie",
    "Response": "True"
  },
  {
    "id": 145487,
    "titulo": "Spider-Man",
    "ano": 2002,
    "classificacao": "PG-13",
    "lancamento": "03 de maio de 2002",
    "duracao": "121 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Sam Raimi",
    "roteirista": "Stan Lee (Marvel comic book), Steve Ditko (Marvel comic book), David Koepp (screenplay)",
    "elenco": "Tobey Maguire, Willem Dafoe, Kirsten Dunst, James Franco",
    "sinopse": "Baseado no personagem de super-heróis da Marvel Comics, esta é uma história de Peter Parker, que é um nerd de colegial. Ele ficou órfão quando criança, intimidado por atletas e não pode confessar sua paixão por sua deslumbrante vizinha Mary Jane Watson. Dizer que sua vida é \"miserável\" é um eufemismo. Mas um dia, durante uma excursão a um laboratório, uma aranha radioativa em fuga o morde ... e sua vida muda de uma maneira que ninguém poderia imaginar. Peter adquire um físico musculoso, visão clara, capacidade de se agarrar às superfícies e rastejar pelas paredes, tirando teias do pulso ... mas a diversão não vai durar. Um milionário excêntrico, Norman Osborn, administra uma droga para melhorar sua performance em si mesmo, e seu alter ego maníaco, Goblin Verde, emerge. Agora Peter Parker tem que se tornar o Homem-Aranha e levar Goblin Verde para a tarefa ... ou então Goblin irá matá-lo. Eles vêm cara a cara e começa a guerra em que apenas um deles sobreviverá no final.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "Nomeado para 2 Oscars. Mais 16 vitórias e 58 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BZDEyN2NhMjgtMjdhNi00MmNlLWE5YTgtZGE4MzNjMTRlMGEwXkEyXkFqcGdeQXVyNDUyOTg3Njg@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.3/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "90%"
      },
      {
        "Source": "Metacritic",
        "Value": "73/100"
      }
    ],
    "Metascore": 73,
    "imdbRating": 7.3,
    "imdbVotes": 617172,
    "Type": "movie",
    "DVD": "01 Nov 2002",
    "BoxOffice": 403706375,
    "Production": "Columbia Pictures",
    "Website": "http://spiderman.sonypictures.com/",
    "Response": "True"
  },
  {
    "id": 3498820,
    "titulo": "Captain America: Civil War",
    "ano": 2016,
    "classificacao": "PG-13",
    "lancamento": "06 de maio de 2016",
    "duracao": "147 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Anthony Russo, Joe Russo",
    "roteirista": "Christopher Markus (screenplay by), Stephen McFeely (screenplay by), Joe Simon (based on the Marvel comics by), Jack Kirby (based on the Marvel comics by)",
    "elenco": "Chris Evans, Robert Downey Jr., Scarlett Johansson, Sebastian Stan",
    "sinopse": "Com muitas pessoas temendo as ações dos super-heróis, o governo decide pressionar pelo Ato de Registro de Heróis, uma lei que limita as ações de um herói. Isso resulta em uma divisão em The Avengers. O Homem de Ferro mantém esta Lei, alegando que suas ações devem ser mantidas em xeque caso contrário as cidades continuarão a ser destruídas, mas o Capitão América acha que salvar o mundo é ousado o suficiente e que não pode confiar no governo para proteger o mundo. Isso se transforma em uma guerra total entre o Homem de Ferro da Equipe (Homem de Ferro, Pantera Negra, Visão, Viúva Negra, Máquina de Guerra e Homem-Aranha) ea Equipe Capitão América (Capitão América, Bucky Barnes, Falcão, Feiticeira Escarlate, Hawkeye, e Ant Man) enquanto surge um novo vilão.",
    "idioma": "Inglês, alemão, xhosa, russo, romeno, hindi",
    "pais": "EUA, Alemanha",
    "premiacoes": "16 vitórias e 65 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjQ0MTgyNjAxMV5BMl5BanBnXkFtZTgwNjUzMDkyODE@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.8/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "91%"
      },
      {
        "Source": "Metacritic",
        "Value": "75/100"
      }
    ],
    "Metascore": 75,
    "imdbRating": 7.8,
    "imdbVotes": 526979,
    "Type": "movie",
    "DVD": "13 Sep 2016",
    "BoxOffice": 408080554,
    "Production": "Walt Disney Pictures",
    "Website": "http://marvel.com/captainamerica#/home",
    "Response": "True"
  },
  {
    "id": 96895,
    "titulo": "Batman",
    "ano": 1989,
    "classificacao": "PG-13",
    "lancamento": "23 de junho de 1989",
    "duracao": "126 min",
    "genero": "Ação e aventura",
    "diretor": "Tim Burton",
    "roteirista": "Bob Kane (Batman characters), Sam Hamm (story), Sam Hamm (screenplay), Warren Skaaren (screenplay)",
    "elenco": "Michael Keaton, Jack Nicholson, Kim Basinger, Robert Wuhl",
    "sinopse": "Cidade de Gotham. O chefe do crime Carl Grissom (Jack Palance) administra a cidade com eficácia, mas há um novo combatente do crime na cidade - Batman (Michael Keaton). O braço direito de Grissom é Jack Napier (Jack Nicholson), um homem brutal que não é totalmente são ... Depois de cair entre os dois Grissom tem Napier criado com a polícia e Napier cai para sua aparente morte em um tonel de produtos químicos . No entanto, ele logo reaparece como The Joker e começa um reinado de terror em Gotham City. Enquanto isso, a repórter Vicki Vale (Kim Basinger) está na cidade para fazer um artigo sobre o Batman. Ela logo começa um relacionamento com o personagem cotidiano de Batman, o bilionário Bruce Wayne.",
    "idioma": "Inglês, francês, espanhol",
    "pais": "EUA, Reino Unido",
    "premiacoes": "Ganhou 1 Oscar. Mais 8 vitórias e 26 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTYwNjAyODIyMF5BMl5BanBnXkFtZTYwNDMwMDk2._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.6/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "72%"
      },
      {
        "Source": "Metacritic",
        "Value": "69/100"
      }
    ],
    "Metascore": 69,
    "imdbRating": 7.6,
    "imdbVotes": 303988,
    "Type": "movie",
    "DVD": "25 Mar 1997",
    "BoxOffice": "N/D",
    "Production": "Warner Bros. Pictures",
    "Website": "N/D",
    "Response": "True"
  },
  {
    "id": 287978,
    "titulo": "Daredevil",
    "ano": 2003,
    "classificacao": "PG-13",
    "lancamento": "14 de fevereiro de 2003",
    "duracao": "103 min",
    "genero": "Ação, Crime",
    "diretor": "Mark Steven Johnson",
    "roteirista": "Mark Steven Johnson (screenplay)",
    "elenco": "Ben Affleck, Jennifer Garner, Colin Farrell, Michael Clarke Duncan",
    "sinopse": "O destino trata o jovem órfão Matt Murdock de uma mão estranha quando está mergulhado em lixo perigoso. O acidente deixa Matt cego, mas também lhe dá um \"senso de radar\" que lhe permite \"ver\" muito melhor do que qualquer homem. Anos depois, Murdock tornou-se um homem e se torna um respeitado advogado criminal. Mas depois que ele faz seu \"trabalho diário\", Matt assume uma identidade secreta como \"O Homem Sem Medo\", Demolidor, o vingador mascarado que patrulha o bairro de Hell's Kitchen e Nova York para combater a injustiça que ele não pode enfrentar na sala de audiências. .",
    "idioma": "Inglês, grego, italiano",
    "pais": "EUA",
    "premiacoes": "5 vitórias e 16 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjYwZDNhMTgtNjEwNS00Y2Y0LTkxYjMtY2MyYTM0NDE1N2ZlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "5.3/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "44%"
      },
      {
        "Source": "Metacritic",
        "Value": "42/100"
      }
    ],
    "Metascore": 42,
    "imdbRating": 5.3,
    "imdbVotes": 193603,
    "Type": "movie",
    "DVD": "29 Jul 2003",
    "BoxOffice": 102469640,
    "Production": "20th Century Fox",
    "Website": "http://www.daredevilmovie.com",
    "Response": "True"
  },
  {
    "id": 800369,
    "titulo": "Thor",
    "ano": 2011,
    "classificacao": "PG-13",
    "lancamento": "06 de maio de 2011",
    "duracao": "115 min",
    "genero": "Ação, Aventura, Fantasia, Ficção Científica",
    "diretor": "Kenneth Branagh",
    "roteirista": "Ashley Miller (screenplay), Zack Stentz (screenplay), Don Payne (screenplay), J. Michael Straczynski (story), Mark Protosevich (story), Stan Lee (comic book), Larry Lieber (comic book), Jack Kirby (comic book)",
    "elenco": "Chris Hemsworth, Natalie Portman, Tom Hiddleston, Anthony Hopkins",
    "sinopse": "O guerreiro Thor (Hemsworth) é expulso do reino fantástico de Asgard por seu pai Odin (Hopkins) por sua arrogância e enviado para a Terra para viver entre os humanos. Apaixonar-se pela cientista Jane Foster (Portman) ensina as tão necessárias lições de Thor, e sua força recém-descoberta entra em jogo quando um vilão de sua terra natal envia forças obscuras para a Terra.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "5 vitórias e 30 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BOGE4NzU1YTAtNzA3Mi00ZTA2LTg2YmYtMDJmMThiMjlkYjg2XkEyXkFqcGdeQXVyNTgzMDMzMTg@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.0/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "77%"
      },
      {
        "Source": "Metacritic",
        "Value": "57/100"
      }
    ],
    "Metascore": 57,
    "imdbRating": 7.0,
    "imdbVotes": 641965,
    "Type": "movie",
    "DVD": "13 Sep 2011",
    "BoxOffice": 181015141,
    "Production": "Paramount Pictures",
    "Website": "http://thor.marvel.com/",
    "Response": "True"
  },
  {
    "id": 458339,
    "titulo": "Captain America: The First Avenger",
    "ano": 2011,
    "classificacao": "PG-13",
    "lancamento": "22 de julho de 2011",
    "duracao": "124 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Joe Johnston",
    "roteirista": "Christopher Markus (screenplay), Stephen McFeely (screenplay), Joe Simon (comic books), Jack Kirby (comic books)",
    "elenco": "Chris Evans, Hayley Atwell, Sebastian Stan, Tommy Lee Jones",
    "sinopse": "É 1942, a América entrou na Segunda Guerra Mundial, e o doente, mas determinado, Steve Rogers está frustrado por ter sido novamente rejeitado pelo serviço militar. Tudo muda quando o Dr. Erskine o recruta para o Projeto Renascimento secreto. Provando sua extraordinária coragem, inteligência e consciência, Rogers é submetido à experiência e seu corpo fraco é repentinamente aumentado no máximo potencial humano. Quando o Dr. Erskine é imediatamente assassinado por um agente do departamento secreto de pesquisa HYDRA da Alemanha nazista (liderado por Johann Schmidt, também conhecido como Caveira Vermelha), Rogers é deixado como um homem único, inicialmente usado como mascote de propaganda; no entanto, quando seus companheiros precisam dele, Rogers inicia uma aventura de sucesso que realmente faz dele o Capitão América, e sua guerra contra Schmidt começa.",
    "idioma": "Inglês, norueguês, francês",
    "pais": "EUA",
    "premiacoes": "3 vitórias e 46 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTYzOTc2NzU3N15BMl5BanBnXkFtZTcwNjY3MDE3NQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.9/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "79%"
      },
      {
        "Source": "Metacritic",
        "Value": "66/100"
      }
    ],
    "Metascore": 66,
    "imdbRating": 6.9,
    "imdbVotes": 619892,
    "Type": "movie",
    "DVD": "25 Oct 2011",
    "BoxOffice": 176636816,
    "Production": "Paramount Pictures",
    "Website": "http://captainamerica.marvel.com/",
    "Response": "True"
  },
  {
    "id": 103776,
    "titulo": "Batman Returns",
    "ano": 1992,
    "classificacao": "PG-13",
    "lancamento": "19 de junho de 1992",
    "duracao": "126 min",
    "genero": "Ação, crime, fantasia",
    "diretor": "Tim Burton",
    "roteirista": "Bob Kane (Batman characters), Daniel Waters (story), Sam Hamm (story), Daniel Waters (screenplay)",
    "elenco": "Michael Keaton, Danny DeVito, Michelle Pfeiffer, Christopher Walken",
    "sinopse": "Nos esgotos da cidade de Gotham para os telhados da cidade de Gotham, o pinguim quer saber de onde ele veio bem em seu vilão A mulher-gato planeja matar homem rico de Gotham Max Shreak Mas como ele batalhas com o milionário Bruce Wayne ambos os homens têm suas segredos próprios Bruce Wayne está de volta como Homem morcego tentando parar o pinguim Max está ajudando pinguim a roubar cidade de Gotham enquanto selina Kyle / mulher-gato tenta ajudar o pinguim a não conhecer seu alvo de assassinato também seu assassinato está ajudando ele mas todos os quatro homens têm seus objetivos Gotham do crime ganhando o assassinato de Gotham City por dois homens e mais dinheiro para ser Gotham Citys número um homem rico.",
    "idioma": "Inglês",
    "pais": "EUA, Reino Unido",
    "premiacoes": "Nomeado para 2 Oscars. Mais 2 vitórias e 27 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BOGZmYzVkMmItM2NiOS00MDI3LWI4ZWQtMTg0YWZkODRkMmViXkEyXkFqcGdeQXVyODY0NzcxNw@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.0/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "79%"
      },
      {
        "Source": "Metacritic",
        "Value": "68/100"
      }
    ],
    "Metascore": 68,
    "imdbRating": 7.0,
    "imdbVotes": 244535,
    "Type": "movie",
    "DVD": "29 Apr 1997",
    "BoxOffice": "N/D",
    "Production": "Warner Bros. Pictures",
    "Website": "N/D",
    "Response": "True"
  },
  {
    "id": 770828,
    "titulo": "Man of Steel",
    "ano": 2013,
    "classificacao": "PG-13",
    "lancamento": "14 de junho de 2013",
    "duracao": "143 min",
    "genero": "Ação, aventura, fantasia",
    "diretor": "Zack Snyder",
    "roteirista": "David S. Goyer (screenplay), David S. Goyer (story), Christopher Nolan (story), Jerry Siegel (Superman created by), Joe Shuster (Superman created by)",
    "elenco": "Henry Cavill, Amy Adams, Michael Shannon, Diane Lane",
    "sinopse": "Um jovem menino aprende que ele tem poderes extraordinários e não é desta Terra. Quando jovem, ele viaja para descobrir de onde veio e para o que foi enviado para fazer. Mas o herói nele deve emergir se quiser salvar o mundo da aniquilação e se tornar o símbolo de esperança para toda a humanidade.",
    "idioma": "Inglês",
    "pais": "EUA, Reino Unido",
    "premiacoes": "7 vitórias e 46 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTk5ODk1NDkxMF5BMl5BanBnXkFtZTcwNTA5OTY0OQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.1/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "55%"
      },
      {
        "Source": "Metacritic",
        "Value": "55/100"
      }
    ],
    "Metascore": 55,
    "imdbRating": 7.1,
    "imdbVotes": 624186,
    "Type": "movie",
    "DVD": "12 Nov 2013",
    "BoxOffice": 291021565,
    "Production": "Warner Bros. Pictures",
    "Website": "http://manofsteel.warnerbros.com",
    "Response": "True"
  },
  {
    "id": 1300854,
    "titulo": "Iron Man 3",
    "ano": 2013,
    "classificacao": "PG-13",
    "lancamento": "03 de maio de 2013",
    "duracao": "130 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Shane Black",
    "roteirista": "Drew Pearce (screenplay by), Shane Black (screenplay by), Stan Lee (based on the Marvel comic book by), Don Heck (based on the Marvel comic book by), Larry Lieber (based on the Marvel comic book by), Jack Kirby (based on the Marvel comic book by), Warren Ellis (based on the \"Extremis\" mini-series written by), Adi Granov (based on the \"Extremis\" mini-series illustrated by)",
    "elenco": "Robert Downey Jr., Gwyneth Paltrow, Don Cheadle, Guy Pearce",
    "sinopse": "\"Homem de Ferro 3\", da Marvel, opõe o impetuoso e brilhante industrial Tony Stark / Homem de Ferro a um inimigo cujo alcance não conhece fronteiras. Quando Stark encontra seu mundo pessoal destruído nas mãos de seu inimigo, ele embarca em uma busca angustiante para encontrar os responsáveis. Esta jornada, a todo momento, testará sua coragem. Com as costas contra a parede, Stark é deixado para sobreviver por seus próprios dispositivos, contando com sua ingenuidade e instintos para proteger as pessoas mais próximas a ele. Enquanto ele luta de volta, Stark descobre a resposta para a pergunta que o assombra: o homem faz o terno ou o terno faz o homem?",
    "idioma": "Inglês",
    "pais": "China, EUA",
    "premiacoes": "Nomeado para 1 Oscar. Mais 17 vitórias e 61 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjE5MzcyNjk1M15BMl5BanBnXkFtZTcwMjQ4MjcxOQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.2/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "80%"
      },
      {
        "Source": "Metacritic",
        "Value": "62/100"
      }
    ],
    "Metascore": 62,
    "imdbRating": 7.2,
    "imdbVotes": 652694,
    "Type": "movie",
    "DVD": "24 Sep 2013",
    "BoxOffice": 408992272,
    "Production": "Walt Disney Pictures",
    "Website": "http://IronManMovie3.com",
    "Response": "True"
  },
  {
    "id": 413300,
    "titulo": "Spider-Man 3",
    "ano": 2007,
    "classificacao": "PG-13",
    "lancamento": "04 de maio de 2007",
    "duracao": "139 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Sam Raimi",
    "roteirista": "Sam Raimi (screenplay), Ivan Raimi (screenplay), Alvin Sargent (screenplay), Sam Raimi (screen story), Ivan Raimi (screen story), Stan Lee (Marvel comic book), Steve Ditko (Marvel comic book)",
    "elenco": "Tobey Maguire, Kirsten Dunst, James Franco, Thomas Haden Church",
    "sinopse": "Peter Parker finalmente conseguiu juntar as partes da sua vida uma vez quebradas, mantendo um equilíbrio entre seu relacionamento com Mary-Jane e sua responsabilidade como Homem-Aranha. Mas novos desafios surgem para nosso jovem herói. O velho amigo de Peter, Harry Obsourne, decidiu se vingar de Peter; assumindo o manto da personalidade de seu falecido pai como O Novo Duende, e Peter também deve capturar o verdadeiro assassino de Tio Ben, Flint Marko, que foi transformado em seu mais forte inimigo, o Sandman. Toda a esperança parece perdida quando, de repente, o terno de Peter se torna negro e amplifica enormemente seus poderes. Mas também começa a ampliar bastante as qualidades mais sombrias da personalidade de Peter, às quais ele começa a se perder. Pedro tem que ir fundo dentro de si mesmo para libertar o herói compassivo que costumava ser se quisesse conquistar a escuridão interior e enfrentar não apenas seus maiores inimigos, mas também ... ele mesmo.",
    "idioma": "Inglês francês",
    "pais": "EUA",
    "premiacoes": "Indicado para 1 prêmio de filme BAFTA. Mais 3 vitórias e 32 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BYTk3MDljOWQtNGI2My00OTEzLTlhYjQtOTQ4ODM2MzUwY2IwXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.2/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "63%"
      },
      {
        "Source": "Metacritic",
        "Value": "59/100"
      }
    ],
    "Metascore": 59,
    "imdbRating": 6.2,
    "imdbVotes": 443597,
    "Type": "movie",
    "DVD": "30 Oct 2007",
    "BoxOffice": 336530303,
    "Production": "Sony Pictures",
    "Website": "http://www.sonypictures.com/movies/spiderman3/site/",
    "Response": "True"
  },
  {
    "id": 120903,
    "titulo": "X-Men",
    "ano": 2000,
    "classificacao": "PG-13",
    "lancamento": "14 de julho de 2000",
    "duracao": "104 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Bryan Singer",
    "roteirista": "Tom DeSanto (story), Bryan Singer (story), David Hayter (screenplay)",
    "elenco": "Hugh Jackman, Patrick Stewart, Ian McKellen, Famke Janssen",
    "sinopse": "Em um mundo onde Mutantes e Humanos temem um ao outro, Marie, mais conhecida como Vampira, foge de casa e pega carona com outro mutante, conhecido como Logan, também conhecido como Wolverine. Charles Xavier, dono de uma escola para jovens mutantes, envia Storm e Cyclops para trazê-los de volta antes que seja tarde demais. Magneto, que acredita que uma guerra está se aproximando, tem um plano maligno em mente e precisa do jovem Rogue para ajudá-lo.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "13 vitórias e 26 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BZmJkOGY4YjYtM2FmYy00MTIyLTg2YTUtMzJiNjljMWM5MGUwXkEyXkFqcGdeQXVyNjExODE1MDc@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.4/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "82%"
      },
      {
        "Source": "Metacritic",
        "Value": "64/100"
      }
    ],
    "Metascore": 64,
    "imdbRating": 7.4,
    "imdbVotes": 518827,
    "Type": "movie",
    "DVD": "21 Nov 2000",
    "BoxOffice": 156164829,
    "Production": "20th Century Fox",
    "Website": "http://www.x-men-the-movie.com",
    "Response": "True"
  },
  {
    "id": 118688,
    "titulo": "Batman & Robin",
    "ano": 1997,
    "classificacao": "PG-13",
    "lancamento": "20 de junho de 1997",
    "duracao": "125 min",
    "genero": "Ação, Ficção Científica",
    "diretor": "Joel Schumacher",
    "roteirista": "Bob Kane (Batman characters), Akiva Goldsman",
    "elenco": "Arnold Schwarzenegger, George Clooney, Chris O'Donnell, Uma Thurman",
    "sinopse": "Batman e Robin estão de volta trabalhando lado a lado para parar os vilões de Gotham City, mas há tensão aparecendo entre eles, especialmente quando uma vilã que se chama de Poison Ivy pode fazer alguém se apaixonar por ela ... literalmente. Junto com Poison Ivy, o gelado Mr. Freeze está congelando qualquer coisa que atrapalhe seu objetivo.",
    "idioma": "Inglês",
    "pais": "EUA, Reino Unido",
    "premiacoes": "10 vitórias e 21 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMGQ5YTM1NmMtYmIxYy00N2VmLWJhZTYtN2EwYTY3MWFhOTczXkEyXkFqcGdeQXVyNTA2NTI0MTY@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "3.7/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "10%"
      },
      {
        "Source": "Metacritic",
        "Value": "28/100"
      }
    ],
    "Metascore": 28,
    "imdbRating": 3.7,
    "imdbVotes": 211828,
    "Type": "movie",
    "DVD": "21 Oct 1997",
    "BoxOffice": "N/D",
    "Production": "Warner Home Video",
    "Website": "N/D",
    "Response": "True"
  },
  {
    "id": 1270798,
    "titulo": "X-Men: First Class",
    "ano": 2011,
    "classificacao": "PG-13",
    "lancamento": "03 de junho de 2011",
    "duracao": "131 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Matthew Vaughn",
    "roteirista": "Ashley Miller (screenplay by), Zack Stentz (screenplay by), Jane Goldman (screenplay by), Matthew Vaughn (screenplay by), Sheldon Turner (story by), Bryan Singer (story by)",
    "elenco": "James McAvoy, Laurence Belcher, Michael Fassbender, Bill Milner",
    "sinopse": "Antes de Charles Xavier e Erik Lensherr tomarem os nomes Professor X e Magneto, eles eram dois jovens descobrindo seus poderes pela primeira vez. Antes de serem arquiinimigos, eles eram os amigos mais próximos, trabalhando juntos, com outros Mutantes (alguns familiares, alguns novos), para impedir a maior ameaça que o mundo já conheceu. No processo, abriu-se uma brecha entre eles, que iniciou a eterna guerra entre a Fraternidade de Magneto e os X-MEN do Professor X.",
    "idioma": "Inglês, alemão, francês, espanhol, russo",
    "pais": "EUA, Reino Unido",
    "premiacoes": "20 vitórias e 38 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTg5OTMxNzk4Nl5BMl5BanBnXkFtZTcwOTk1MjAwNQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.7/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "86%"
      },
      {
        "Source": "Metacritic",
        "Value": "65/100"
      }
    ],
    "Metascore": 65,
    "imdbRating": 7.7,
    "imdbVotes": 588161,
    "Type": "movie",
    "DVD": "09 Sep 2011",
    "BoxOffice": 145300000,
    "Production": "20th Century Fox",
    "Website": "http://www.x-menfirstclassmovie.com/",
    "Response": "True"
  },
  {
    "id": 458525,
    "titulo": "X-Men Origins: Wolverine",
    "ano": 2009,
    "classificacao": "PG-13",
    "lancamento": "01 de maio de 2009",
    "duracao": "107 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Gavin Hood",
    "roteirista": "David Benioff (screenplay), Skip Woods (screenplay)",
    "elenco": "Hugh Jackman, Liev Schreiber, Danny Huston, Will.i.am",
    "sinopse": "Dois irmãos mutantes, Logan e Victor, nascidos há 200 anos, sofrem traumas de infância e dependem um do outro. Basicamente, eles são combatentes e assassinos, vivendo da guerra para a guerra através da história dos EUA. Nos tempos modernos, um coronel americano, Stryker, recruta-os e outros mutantes como comandos. Logan se demite e se torna um madeireiro, se apaixonando por um professor local. Quando Logan se recusa a se juntar à tripulação de Stryker, o coronel envia o assassino Victor. Logan agora quer vingança.",
    "idioma": "Inglês",
    "pais": "EUA, Reino Unido",
    "premiacoes": "3 vitórias e 18 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BZWRhMzdhMzEtZTViNy00YWYyLTgxZmUtMTMwMWM0NTEyMjk3XkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.6/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "37%"
      },
      {
        "Source": "Metacritic",
        "Value": "40/100"
      }
    ],
    "Metascore": 40,
    "imdbRating": 6.6,
    "imdbVotes": 419852,
    "Type": "movie",
    "DVD": "15 Sep 2009",
    "BoxOffice": 179737340,
    "Production": "20th Century Fox",
    "Website": "http://www.x-menorigins.com/",
    "Response": "True"
  },
  {
    "id": 112462,
    "titulo": "Batman Forever",
    "ano": 1995,
    "classificacao": "PG-13",
    "lancamento": "16 de junho de 1995",
    "duracao": "121 min",
    "genero": "Ação, aventura, fantasia",
    "diretor": "Joel Schumacher",
    "roteirista": "Bob Kane (characters), Lee Batchler (story), Janet Scott Batchler (story), Lee Batchler (screenplay), Janet Scott Batchler (screenplay), Akiva Goldsman (screenplay)",
    "elenco": "Val Kilmer, Tommy Lee Jones, Jim Carrey, Nicole Kidman",
    "sinopse": "O Cavaleiro das Trevas de Gotham City confronta uma dupla covarde: Two-Face e Riddler. Ex-promotor público Harvey Dent, Two-Face acredita erroneamente que Batman causou o acidente no tribunal que o deixou desfigurado de um lado; ele desencadeou um reinado de terror sobre as boas pessoas de Gotham. Edward Nygma, gênio da computação e ex-funcionário do milionário Bruce Wayne, está à procura do filantropo; como The Riddler ele aperfeiçoa um dispositivo para drenar informações de todos os cérebros em Gotham, incluindo o conhecimento de Bruce Wayne de sua outra identidade. Batman / Wayne é o foco do amor do Dr. Chase Meridan. O ex-acrobata de circo Dick Grayson, sua família morta por Two-Face, torna-se o protegido de Wayne e o novo parceiro de Batman, Robin the Boy Wonder.",
    "idioma": "Inglês",
    "pais": "EUA, Reino Unido",
    "premiacoes": "Nomeado para 3 Oscars. Mais 10 vitórias e 22 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNWY3M2I0YzItNzA1ZS00MzE3LThlYTEtMTg2YjNiOTYzODQ1XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "5.4/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "40%"
      },
      {
        "Source": "Metacritic",
        "Value": "51/100"
      }
    ],
    "Metascore": 51,
    "imdbRating": 5.4,
    "imdbVotes": 214275,
    "Type": "movie",
    "DVD": "27 Aug 1997",
    "BoxOffice": "N/D",
    "Production": "Warner Bros. Pictures",
    "Website": "N/D",
    "Response": "True"
  },
  {
    "id": 4682266,
    "titulo": "The New Mutants",
    "ano": 2019,
    "classificacao": "N/D",
    "lancamento": "02 de agosto de 2019",
    "duracao": "N/D",
    "genero": "Ação, Terror, Ficção Científica",
    "diretor": "Josh Boone",
    "roteirista": "Josh Boone, Knate Lee, Chris Claremont (based on the Marvel comics by), Bob McLeod (based on the Marvel comics by), Len Wein (character created by: Magik), Dave Cockrum (character created by: Magik)",
    "elenco": "Anya Taylor-Joy, Maisie Williams, Alice Braga, Antonio Banderas",
    "sinopse": "Cinco jovens mutantes, descobrindo suas habilidades enquanto estão em uma instalação secreta contra sua vontade, lutam para escapar de seus pecados passados ​​e se salvar.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "N/D",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTgxNzM1NzExM15BMl5BanBnXkFtZTgwODMwMDIzNDM@._V1_SX300.jpg",
    "Ratings": [],
    "Metascore": "N/D",
    "imdbRating": "N/D",
    "imdbVotes": "N/D",
    "Type": "movie",
    "DVD": "N/D",
    "BoxOffice": "N/D",
    "Production": "N/D",
    "Website": "N/D",
    "Response": "True"
  },
  {
    "id": 1877832,
    "titulo": "X-Men: Days of Future Past",
    "ano": 2014,
    "classificacao": "PG-13",
    "lancamento": "23 de maio de 2014",
    "duracao": "132 min",
    "genero": "Ação, Aventura, Ficção Científica, Suspense",
    "diretor": "Bryan Singer",
    "roteirista": "Simon Kinberg (screenplay by), Jane Goldman (story by), Simon Kinberg (story by), Matthew Vaughn (story by)",
    "elenco": "Hugh Jackman, James McAvoy, Michael Fassbender, Jennifer Lawrence",
    "sinopse": "No futuro, os mutantes e os humanos que os ajudam são abatidos por robôs poderosos chamados Sentinelas. Professor Xavier, Wolverine, Magneto, Tempestade, Kitty Pryde e suas amigas se encontram em um monastério na China e Xavier explica que os invencíveis Sentinelas foram criados usando o DNA de Mystique que foi capturado em 1973 quando ela tentou assassinar seu criador Dr. Bolivar Trask . Xavier conta que a sua única chance é voltar a 1973 usando a habilidade de Pryde para se juntar a Charles Xavier e Erik Lehnsherr para convencer Mystique a desistir de sua intenção. . No entanto, apenas o Wolverine pode suportar os danos da viagem no tempo. Será que ele conseguirá impedir Mystique e o Sentinel Program e salvar os mutantes e seus amigos humanos da aniquilação?",
    "idioma": "Inglês, vietnamita, francês",
    "pais": "EUA, Reino Unido, Canadá",
    "premiacoes": "Nomeado para 1 Oscar. Mais 15 vitórias e 47 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BZGIzNWYzN2YtMjcwYS00YjQ3LWI2NjMtOTNiYTUyYjE2MGNkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "8.0/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "89%"
      },
      {
        "Source": "Metacritic",
        "Value": "75/100"
      }
    ],
    "Metascore": 75,
    "imdbRating": 8.0,
    "imdbVotes": 597256,
    "Type": "movie",
    "DVD": "14 Oct 2014",
    "BoxOffice": 199305306,
    "Production": "20th Century Fox",
    "Website": "http://www.x-menmovies.com/",
    "Response": "True"
  },
  {
    "id": 1843866,
    "titulo": "Captain America: The Winter Soldier",
    "ano": 2014,
    "classificacao": "PG-13",
    "lancamento": "04 de abril de 2014",
    "duracao": "136 min",
    "genero": "Ação, Aventura, Ficção Científica, Suspense",
    "diretor": "Anthony Russo, Joe Russo",
    "roteirista": "Christopher Markus (screenplay by), Stephen McFeely (screenplay by), Joe Simon (based on the Marvel comics by), Jack Kirby (based on the Marvel comics by)",
    "elenco": "Chris Evans, Samuel L. Jackson, Scarlett Johansson, Robert Redford",
    "sinopse": "Para Steve Rogers, o despertar depois de décadas de animação suspensa envolve mais do que pôr em dia a cultura pop; Isso também significa que esse idealista da velha escola deve enfrentar um mundo de ameaças mais sutis e complexidades morais difíceis. Isso fica claro quando o diretor Nick Fury é morto pelo misterioso assassino, o Soldado Invernal, mas não antes de avisar a Rogers que a SHIELD foi subvertida por seus inimigos. Quando Rogers atua no alerta de Fúria para não confiar em ninguém, ele é considerado traidor pela organização. Agora um fugitivo, o Capitão América deve chegar ao fundo desse mistério mortal com a ajuda da Viúva Negra e seu novo amigo, O Falcão. No entanto, a batalha será custosa para o Sentinel of Liberty, com Rogers encontrando inimigos onde ele menos espera enquanto aprende que o Winter Soldier parece perturbadoramente familiar.",
    "idioma": "Inglês francês",
    "pais": "EUA",
    "premiacoes": "Nomeado para 1 Oscar. Mais 5 vitórias e 50 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMzA2NDkwODAwM15BMl5BanBnXkFtZTgwODk5MTgzMTE@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.8/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "89%"
      },
      {
        "Source": "Metacritic",
        "Value": "70/100"
      }
    ],
    "Metascore": 70,
    "imdbRating": 7.8,
    "imdbVotes": 618035,
    "Type": "movie",
    "DVD": "09 Sep 2014",
    "BoxOffice": 228636083,
    "Production": "Walt Disney Pictures",
    "Website": "http://marvel.com/captainamerica",
    "Response": "True"
  },
  {
    "id": 1872181,
    "titulo": "The Amazing Spider-Man 2",
    "ano": 2014,
    "classificacao": "PG-13",
    "lancamento": "02 de maio de 2014",
    "duracao": "142 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Marc Webb",
    "roteirista": "Alex Kurtzman (screenplay), Roberto Orci (screenplay), Jeff Pinkner (screenplay), Alex Kurtzman (screen story), Roberto Orci (screen story), Jeff Pinkner (screen story), James Vanderbilt (screen story), Stan Lee (Marvel comic book), Steve Ditko (Marvel comic book)",
    "elenco": "Andrew Garfield, Emma Stone, Jamie Foxx, Dane DeHaan",
    "sinopse": "Nós sempre soubemos que o conflito mais importante do Homem-Aranha tem sido dentro dele: a luta entre as obrigações comuns de Peter Parker e as extraordinárias responsabilidades do Homem-Aranha. Mas em The Amazing Spider-Man 2, Peter Parker descobre que sua maior batalha está prestes a começar. É ótimo ser o Homem-Aranha. Para Peter Parker, não há nada como balançar entre os arranha-céus, abraçar ser o herói e passar tempo com Gwen. Mas ser Homem-Aranha tem um preço: somente o Homem-Aranha pode proteger seus companheiros nova-iorquinos dos formidáveis ​​vilões que ameaçam a cidade. Com o surgimento de Electro, Peter deve confrontar um inimigo muito mais poderoso que ele. E quando seu velho amigo, Harry Osborn, retorna, Peter percebe que todos os seus inimigos têm uma coisa em comum: a Oscorp.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "3 vitórias e 29 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BOTA5NDYxNTg0OV5BMl5BanBnXkFtZTgwODE5NzU1MTE@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.6/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "52%"
      },
      {
        "Source": "Metacritic",
        "Value": "53/100"
      }
    ],
    "Metascore": 53,
    "imdbRating": 6.6,
    "imdbVotes": 373931,
    "Type": "movie",
    "DVD": "19 Aug 2014",
    "BoxOffice": 183277573,
    "Production": "Sony Pictures",
    "Website": "http://www.theamazingspiderman.com/site/",
    "Response": "True"
  },
  {
    "id": 78346,
    "titulo": "Superman",
    "ano": 1978,
    "classificacao": "PG",
    "lancamento": "15 de dezembro de 1978",
    "duracao": "143 min",
    "genero": "Ação, Aventura, Drama, Ficção Científica",
    "diretor": "Richard Donner",
    "roteirista": "Jerry Siegel (character created by: Superman), Joe Shuster (character created by: Superman), Mario Puzo (story), Mario Puzo (screenplay), David Newman (screenplay), Leslie Newman (screenplay), Robert Benton (screenplay)",
    "elenco": "Marlon Brando, Gene Hackman, Christopher Reeve, Ned Beatty",
    "sinopse": "Pouco antes da destruição do planeta Krypton, o cientista Jor-El envia seu filho recém-nascido Kal-El em uma nave espacial para a Terra. Criado por gentis agricultores Jonathan e Martha Kent, o jovem Clark descobre a fonte de seus poderes sobre-humanos e se muda para Metropolis para combater o mal. Como Superman, ele luta contra o vilão Lex Luthor, enquanto, como o novato Clark Kent, ele tenta atrair a colega de trabalho Lois Lane.",
    "idioma": "Inglês",
    "pais": "EUA, Reino Unido, Suíça, Canadá, Panamá",
    "premiacoes": "Nomeado para 3 Oscars. Mais 17 vitórias e 18 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMzA0YWMwMTUtMTVhNC00NjRkLWE2ZTgtOWEzNjJhYzNiMTlkXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.3/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "94%"
      },
      {
        "Source": "Metacritic",
        "Value": "80/100"
      }
    ],
    "Metascore": 80,
    "imdbRating": 7.3,
    "imdbVotes": 143472,
    "Type": "movie",
    "DVD": "01 May 2001",
    "BoxOffice": "N/D",
    "Production": "Warner Bros. Pictures",
    "Website": "http://www2.warnerbros.com/superman/home.html",
    "Response": "True"
  },
  {
    "id": 800080,
    "titulo": "The Incredible Hulk",
    "ano": 2008,
    "classificacao": "PG-13",
    "lancamento": "13 de junho de 2008",
    "duracao": "112 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Louis Leterrier",
    "roteirista": "Zak Penn (screenplay), Zak Penn (screen story), Stan Lee (Marvel comic book), Jack Kirby (Marvel comic book)",
    "elenco": "Edward Norton, Liv Tyler, Tim Roth, William Hurt",
    "sinopse": "Representando os eventos após a Bomba Gama. \"O Incrível Hulk\" conta a história do Dr. Bruce Banner, que busca uma cura para sua condição única, que faz com que ele se transforme em um gigante monstro verde sob estresse emocional. Enquanto fugindo das forças armadas que buscam sua captura, Banner chega perto de uma cura. Mas tudo se perde quando uma nova criatura surge; O Abominação",
    "idioma": "Inglês, Português, Espanhol",
    "pais": "EUA",
    "premiacoes": "1 vitória e 8 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTUyNzk3MjA1OF5BMl5BanBnXkFtZTcwMTE1Njg2MQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.8/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "67%"
      },
      {
        "Source": "Metacritic",
        "Value": "61/100"
      }
    ],
    "Metascore": 61,
    "imdbRating": 6.8,
    "imdbVotes": 379565,
    "Type": "movie",
    "DVD": "21 Oct 2008",
    "BoxOffice": 134518390,
    "Production": "Universal Pictures",
    "Website": "http://www.incrediblehulk.com/",
    "Response": "True"
  },
  {
    "id": 1981115,
    "titulo": "Thor: The Dark World",
    "ano": 2013,
    "classificacao": "PG-13",
    "lancamento": "08 de novembro de 2013",
    "duracao": "112 min",
    "genero": "Ação, aventura, fantasia",
    "diretor": "Alan Taylor",
    "roteirista": "Christopher L. Yost (screenplay by), Christopher Markus (screenplay by), Stephen McFeely (screenplay by), Don Payne (story by), Robert Rodat (story by), Stan Lee (based on the Marvel comics by), Larry Lieber (based on the Marvel comics by), Jack Kirby (based on the Marvel comics by), Walter Simonson (character created by: Malekith)",
    "elenco": "Chris Hemsworth, Natalie Portman, Tom Hiddleston, Anthony Hopkins",
    "sinopse": "Milhares de anos atrás, uma raça de seres conhecidos como Dark Elves tentou enviar o universo para a escuridão usando uma arma conhecida como Aether. Guerreiros de Asgard os impedem, mas seu líder Malekith escapa para esperar por outra oportunidade. Os guerreiros encontram o Aether e, como não podem ser destruídos, tentam escondê-lo. Nos dias de hoje, Jane Foster aguarda o retorno de Thor, embora tenha sido dois anos desde a última vez que viram um outro. Enquanto isso, Thor vem tentando trazer paz aos nove reinos. Jane descobre uma anomalia semelhante à que trouxe Thor à Terra. Ela vai investigar, encontra um buraco de minhoca e é sugada por ele. De volta a Asgard, Thor deseja retornar à Terra, mas seu pai, Odin, se recusa a deixá-lo. Thor aprende com Heimdall, que pode ver em todos os reinos, que Jane desapareceu. Thor então retorna à Terra assim como Jane reaparece. No entanto, quando alguns policiais tentam prendê-la, uma energia desconhecida os repele. Thor então traz Jane para Asgard para descobrir o que aconteceu com ela. Quando a energia é liberada novamente, eles descobrem que quando Jane desapareceu, ela cruzou o caminho com o Aether e entrou nela. Malekith, ao perceber que a hora de atacar é agora, procura o Aether. Ele ataca Asgard e a mãe de Thor, Frigga, é morta protegendo Jane. Odin quer manter Jane em Asgard para que Malekith venha. Thor discorda de seu plano, então com seus companheiros ele decide levar Jane embora. Ele pede a ajuda de seu irmão, Loki. Infelizmente, as motivações de Loki permanecem desconhecidas.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "3 vitórias e 21 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTQyNzAwOTUxOF5BMl5BanBnXkFtZTcwMTE0OTc5OQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.0/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "66%"
      },
      {
        "Source": "Metacritic",
        "Value": "54/100"
      }
    ],
    "Metascore": 54,
    "imdbRating": 7.0,
    "imdbVotes": 505108,
    "Type": "movie",
    "DVD": "25 Feb 2014",
    "BoxOffice": 206360018,
    "Production": "Walt Disney Pictures",
    "Website": "http://www.facebook.com/ThorMovie",
    "Response": "True"
  },
  {
    "id": 948470,
    "titulo": "The Amazing Spider-Man",
    "ano": 2012,
    "classificacao": "PG-13",
    "lancamento": "03 de julho de 2012",
    "duracao": "136 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Marc Webb",
    "roteirista": "James Vanderbilt (screenplay), Alvin Sargent (screenplay), Steve Kloves (screenplay), James Vanderbilt (story), Stan Lee (based on the Marvel comic book by), Steve Ditko (based on the Marvel comic book by)",
    "elenco": "Andrew Garfield, Emma Stone, Rhys Ifans, Denis Leary",
    "sinopse": "Peter Parker (Garfield) é um estudante de segundo grau que foi abandonado pelos pais quando era menino, deixando-o para ser criado por seu tio Ben (Sheen) e tia May (campo). Como a maioria dos adolescentes, Peter está tentando descobrir quem ele é e como ele chegou a ser a pessoa que ele é hoje. Peter também está encontrando seu caminho com sua primeira paixão de colegial, Gwen Stacy (Stone), e juntos, eles lutam com amor, compromisso e segredos. Enquanto Peter descobre uma misteriosa pasta que pertenceu a seu pai, ele começa uma busca para entender o desaparecimento de seus pais - levando-o diretamente para a Oscorp e o laboratório do Dr. Curt Connors (Ifans), ex-parceiro de seu pai. Enquanto o Homem-Aranha é colocado em rota de colisão com o alter-ego de Connors, O Lagarto, Peter fará escolhas que alteram a vida para usar seus poderes e moldar seu destino para se tornar um herói.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "2 vitórias e 31 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjMyOTM4MDMxNV5BMl5BanBnXkFtZTcwNjIyNzExOA@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.0/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "72%"
      },
      {
        "Source": "Metacritic",
        "Value": "66/100"
      }
    ],
    "Metascore": 66,
    "imdbRating": 7.0,
    "imdbVotes": 508124,
    "Type": "movie",
    "DVD": "09 Nov 2012",
    "BoxOffice": 262030663,
    "Production": "Sony Pictures",
    "Website": "http://www.theamazingspiderman.com/site/",
    "Response": "True"
  },
  {
    "id": 1228705,
    "titulo": "Iron Man 2",
    "ano": 2010,
    "classificacao": "PG-13",
    "lancamento": "07 de maio de 2010",
    "duracao": "124 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Jon Favreau",
    "roteirista": "Justin Theroux (screenplay), Stan Lee (Marvel comic book), Don Heck (Marvel comic book), Larry Lieber (Marvel comic book), Jack Kirby (Marvel comic book)",
    "elenco": "Robert Downey Jr., Gwyneth Paltrow, Don Cheadle, Scarlett Johansson",
    "sinopse": "Com o mundo agora ciente de sua vida dupla como o super-herói de ferro blindado Iron Man, o bilionário inventor Tony Stark enfrenta pressão do governo, da imprensa e do público para compartilhar sua tecnologia com os militares. Não querendo abandonar sua invenção, Stark, junto com Pepper Potts, e James \"Rhodey\" Rhodes a seu lado, devem forjar novas alianças - e confrontar inimigos poderosos.",
    "idioma": "Inglês, francês, russo",
    "pais": "EUA",
    "premiacoes": "Nomeado para 1 Oscar. Mais 7 vitórias e 42 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTM0MDgwNjMyMl5BMl5BanBnXkFtZTcwNTg3NzAzMw@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.0/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "73%"
      },
      {
        "Source": "Metacritic",
        "Value": "57/100"
      }
    ],
    "Metascore": 57,
    "imdbRating": 7.0,
    "imdbVotes": 616188,
    "Type": "movie",
    "DVD": "28 Sep 2010",
    "BoxOffice": 312057433,
    "Production": "Paramount Studios",
    "Website": "http://www.ironmanmovie.com/",
    "Response": "True"
  },
  {
    "id": 120611,
    "titulo": "Blade",
    "ano": 1998,
    "classificacao": "R",
    "lancamento": "21 de agosto de 1998",
    "duracao": "120 min",
    "genero": "Ação, horror",
    "diretor": "Stephen Norrington",
    "roteirista": "David S. Goyer",
    "elenco": "Wesley Snipes, Stephen Dorff, Kris Kristofferson, N'Bushe Wright",
    "sinopse": "Em um mundo onde os vampiros andam pela terra, Blade tem um objetivo. Seu objetivo é livrar o mundo de todo o mal dos vampiros. Quando Blade testemunha uma mordida de vampiro, Dr. Karen Jenson, ele luta contra a fera e leva Jenson de volta ao seu esconderijo. Aqui, ao lado de Abraham Whistler, Blade tenta ajudar a curar Jenson. O vampiro Quinn que foi atacado por Blade, relata seu mestre Deacon Frost, que está planejando uma grande surpresa para a população humana.",
    "idioma": "Inglês, russo, sérvio",
    "pais": "EUA",
    "premiacoes": "4 vitórias e 8 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTQ4MzkzNjcxNV5BMl5BanBnXkFtZTcwNzk4NTU0Mg@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.1/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "54%"
      },
      {
        "Source": "Metacritic",
        "Value": "45/100"
      }
    ],
    "Metascore": 45,
    "imdbRating": 7.1,
    "imdbVotes": 218782,
    "Type": "movie",
    "DVD": "22 Dec 1998",
    "BoxOffice": "N/D",
    "Production": "New Line Cinema",
    "Website": "N/D",
    "Response": "True"
  },
  {
    "id": 187738,
    "titulo": "Blade II",
    "ano": 2002,
    "classificacao": "R",
    "lancamento": "22 de março de 2002",
    "duracao": "117 min",
    "genero": "Ação, Terror, Ficção Científica, Suspense",
    "diretor": "Guillermo del Toro",
    "roteirista": "Marv Wolfman (character), Gene Colan (character), David S. Goyer",
    "elenco": "Wesley Snipes, Kris Kristofferson, Ron Perlman, Leonor Varela",
    "sinopse": "Uma mutação rara ocorreu dentro da comunidade de vampiros. O ceifeiro. Um vampiro tão consumido com uma sede de sangue insaciável que eles atacam tanto vampiros quanto humanos, transformando vítimas que são infelizes o suficiente para sobreviverem nos próprios Reapers. Agora, sua população em rápida expansão ameaça a existência de vampiros, e logo não haverá humanos suficientes no mundo para satisfazer sua sede de sangue. Blade, Whistler (sim, ele está de volta) e um especialista em arsenais chamado Scud são curiosamente convocados pelo Conselho das Sombras. O conselho relutantemente admite que eles estão em uma situação terrível e eles precisam da ajuda de Blade. Blade então faz uma aliança com o The Bloodpack, uma equipe de elite de vampiros treinados em todos os modos de combate para derrotar a ameaça Reaper. A equipe de Blade e o Bloodpack são a única linha de defesa que pode impedir a população de Reaper de acabar com as populações humanas e de vampiros.",
    "idioma": "Inglês, romeno, checo",
    "pais": "Alemanha, EUA",
    "premiacoes": "6 vitórias e 10 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BOWVjZTIzNDYtNTBlNC00NTJjLTkzOTEtOTE0MjlhYzI2YTcyXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.7/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "57%"
      },
      {
        "Source": "Metacritic",
        "Value": "52/100"
      }
    ],
    "Metascore": 52,
    "imdbRating": 6.7,
    "imdbVotes": 181043,
    "Type": "movie",
    "DVD": "03 Sep 2002",
    "BoxOffice": 82000000,
    "Production": "New Line Cinema",
    "Website": "http://www.blade2.com",
    "Response": "True"
  },
  {
    "id": 348150,
    "titulo": "Superman Returns",
    "ano": 2006,
    "classificacao": "PG-13",
    "lancamento": "28 de junho de 2006",
    "duracao": "154 min",
    "genero": "Ação e aventura",
    "diretor": "Bryan Singer",
    "roteirista": "Michael Dougherty (screenplay), Dan Harris (screenplay), Bryan Singer (story), Michael Dougherty (story), Dan Harris (story), Jerry Siegel (characters), Joe Shuster (characters)",
    "elenco": "Brandon Routh, Kate Bosworth, Kevin Spacey, James Marsden",
    "sinopse": "Após uma misteriosa ausência de vários anos, o Homem de Aço volta à Terra na épica aventura de ação Superman - O Retorno, um novo e crescente capítulo da saga de um dos super-heróis mais adorados do mundo. Enquanto um velho inimigo planeja torná-lo impotente de uma vez por todas, Superman enfrenta a dolorosa percepção de que a mulher que ele ama, Lois Lane, seguiu em frente com sua vida. Ou ela tem? O retorno agridoce do Super-Homem o desafia a diminuir a distância entre eles enquanto encontra um lugar em uma sociedade que aprendeu a sobreviver sem ele. Em uma tentativa de proteger o mundo que ele ama da destruição cataclísmica, Superman embarca em uma jornada épica de redenção que o leva das profundezas do oceano para os confins do espaço sideral.",
    "idioma": "Inglês, alemão, francês",
    "pais": "EUA",
    "premiacoes": "Nomeado para 1 Oscar. Mais 12 vitórias e 40 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNzY2ZDQ2MTctYzlhOC00MWJhLTgxMmItMDgzNDQwMDdhOWI2XkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.1/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "75%"
      },
      {
        "Source": "Metacritic",
        "Value": "72/100"
      }
    ],
    "Metascore": 72,
    "imdbRating": 6.1,
    "imdbVotes": 256260,
    "Type": "movie",
    "DVD": "28 Nov 2006",
    "BoxOffice": 200100000,
    "Production": "Warner Bros. Pictures",
    "Website": "http://www.supermanreturns.com/",
    "Response": "True"
  },
  {
    "id": 376994,
    "titulo": "X-Men: The Last Stand",
    "ano": 2006,
    "classificacao": "PG-13",
    "lancamento": "26 de maio de 2006",
    "duracao": "104 min",
    "genero": "Ação, Aventura, Ficção Científica, Suspense",
    "diretor": "Brett Ratner",
    "roteirista": "Simon Kinberg, Zak Penn",
    "elenco": "Hugh Jackman, Halle Berry, Ian McKellen, Patrick Stewart",
    "sinopse": "Quando uma cura é criada, o que aparentemente pode transformar qualquer mutante em um ser humano normal, há indignação entre a comunidade mutante. Enquanto alguns mutantes gostam da idéia de uma cura, incluindo Rogue, muitos mutantes acham que não deveria haver cura. Magneto, que ainda acredita que a guerra está chegando, recruta uma grande equipe de mutantes para derrubar Warren Worthington II e sua cura. Pode parecer fácil para os X-Men pararem, mas Magneto tem uma grande vantagem, que Wolverine não tem. Jean Gray voltou e se juntou a Magneto. A Fênix acordou dentro dela, que tem a capacidade de destruir qualquer coisa em seu caminho, mesmo que esse 'qualquer coisa' seja um X-Men.",
    "idioma": "Inglês",
    "pais": "Canadá, EUA, Reino Unido",
    "premiacoes": "7 vitórias e 37 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNDBhNDJiMWEtOTg4Yi00NTYzLWEzOGMtMjNmNjAxNTBlMzY3XkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.7/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "58%"
      },
      {
        "Source": "Metacritic",
        "Value": "58/100"
      }
    ],
    "Metascore": 58,
    "imdbRating": 6.7,
    "imdbVotes": 437498,
    "Type": "movie",
    "DVD": "03 Oct 2006",
    "BoxOffice": 234300000,
    "Production": "20th Century Fox",
    "Website": "http://www.x-menthelaststand.com/",
    "Response": "True"
  },
  {
    "id": 290334,
    "titulo": "X-Men 2",
    "ano": 2003,
    "classificacao": "PG-13",
    "lancamento": "02 de maio de 2003",
    "duracao": "134 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Bryan Singer",
    "roteirista": "Zak Penn (story), David Hayter (story), Bryan Singer (story), Michael Dougherty (screenplay), Dan Harris (screenplay), David Hayter (screenplay)",
    "elenco": "Patrick Stewart, Hugh Jackman, Ian McKellen, Halle Berry",
    "sinopse": "Vários meses se passaram desde que os X-Men derrotaram Magneto e o prenderam em uma câmara de plástico aparentemente inexpugnável. Um dia, um mutante chamado Nightcrawler se infiltra na Casa Branca e tenta assassinar o presidente, desencadeando uma reação em cadeia de medidas anti-mutantes por parte do governo. Enquanto isso, Logan está tentando descobrir seu passado. Como o cientista chamado William Stryker descobre a escola secreta do Professor X e Cerebro, a parceira de Magneto, Mystique, está planejando quebrar seu líder fora da prisão. Mas quando a escola do Professor X é atacada pelas forças de Stryker, Logan, Rogue, Iceman e alguns têm sorte de escapar. Aqueles que permanecem se encontram em Boston, onde formam uma aliança desconfortável com Magneto para parar Stryker e resgatar o professor X.",
    "idioma": "Inglês, alemão, italiano, espanhol",
    "pais": "Canadá, EUA",
    "premiacoes": "6 vitórias e 41 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNDk0NjYxMzIzOF5BMl5BanBnXkFtZTYwMTc1MjU3._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.5/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "85%"
      },
      {
        "Source": "Metacritic",
        "Value": "68/100"
      }
    ],
    "Metascore": 68,
    "imdbRating": 7.5,
    "imdbVotes": 463850,
    "Type": "movie",
    "DVD": "25 Nov 2003",
    "BoxOffice": 214813155,
    "Production": "20th Century Fox",
    "Website": "http://www.x2-movie.com/",
    "Response": "True"
  },
  {
    "id": 316654,
    "titulo": "Spider-Man 2",
    "ano": 2004,
    "classificacao": "PG-13",
    "lancamento": "30 de junho de 2004",
    "duracao": "127 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Sam Raimi",
    "roteirista": "Stan Lee (comic book), Steve Ditko (comic book), Alfred Gough (screen story), Miles Millar (screen story), Michael Chabon (screen story), Alvin Sargent (screenplay)",
    "elenco": "Tobey Maguire, Kirsten Dunst, James Franco, Alfred Molina",
    "sinopse": "Peter Parker é um homem infeliz: depois de dois anos lutando contra o crime como Homem-Aranha, sua vida começou a desmoronar. A garota que ele ama está comprometida com outra pessoa, suas notas estão escorregando, ele não consegue manter nenhum de seus empregos e, além disso, o jornal Daily Bugle está atacando-o de maneira cruel, alegando que o Homem-Aranha é um criminoso. Ele atinge o ponto de ruptura e desiste da vida do combatente do crime, de uma vez por todas. Mas depois de um experimento de fusão fracassado, o cientista excêntrico e obsessivo Dr. Otto Octavius ​​é transformado em super vilão Doutor Octopus, abrindo Doc Ock, tendo quatro longos tentáculos como mãos extras. Pedro adivinha que talvez seja a hora do Homem-Aranha retornar, mas ele agiria de acordo com isso?",
    "idioma": "Inglês, russo, chinês",
    "pais": "EUA",
    "premiacoes": "Ganhou 1 Oscar. Mais 23 vitórias e 59 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMzY2ODk4NmUtOTVmNi00ZTdkLTlmOWYtMmE2OWVhNTU2OTVkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "7.3/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "93%"
      },
      {
        "Source": "Metacritic",
        "Value": "83/100"
      }
    ],
    "Metascore": 83,
    "imdbRating": 7.3,
    "imdbVotes": 481586,
    "Type": "movie",
    "DVD": "30 Nov 2004",
    "BoxOffice": 373377893,
    "Production": "Sony Pictures",
    "Website": "http://spiderman.sonypictures.com/",
    "Response": "True"
  },
  {
    "id": 330793,
    "titulo": "The Punisher",
    "ano": 2004,
    "classificacao": "R",
    "lancamento": "16 de abril de 2004",
    "duracao": "124 min",
    "genero": "Ação, Aventura, Crime, Drama, Thriller",
    "diretor": "Jonathan Hensleigh",
    "roteirista": "Jonathan Hensleigh, Michael France",
    "elenco": "A. Russell Andrews, Omar Avila, James Carpinello, Mark Collie",
    "sinopse": "O agente especial Frank Castle tinha tudo: uma família amorosa, uma ótima vida e um trabalho de aventura. Mas quando sua vida é tirada dele por um criminoso implacável e seus associados, Frank renasceu. Agora servindo como juiz, júri e carrasco, ele é um novo tipo de vigilante para travar uma guerra de um homem contra aqueles que o fizeram errado.",
    "idioma": "Inglês",
    "pais": "EUA, Alemanha",
    "premiacoes": "1 vitória e 5 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjI5NjcwMTQxMV5BMl5BanBnXkFtZTcwODg5ODkwNQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.5/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "29%"
      },
      {
        "Source": "Metacritic",
        "Value": "33/100"
      }
    ],
    "Metascore": 33,
    "imdbRating": 6.5,
    "imdbVotes": 139079,
    "Type": "movie",
    "DVD": "07 Sep 2004",
    "BoxOffice": 33682273,
    "Production": "Lions Gate Films",
    "Website": "http://www.punisherthemovie.com/",
    "Response": "True"
  },
  {
    "id": 1502712,
    "titulo": "Fantastic Four",
    "ano": 2015,
    "classificacao": "PG-13",
    "lancamento": "07 de agosto de 2015",
    "duracao": "100 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Josh Trank",
    "roteirista": "Jeremy Slater (screenplay), Simon Kinberg (screenplay), Josh Trank (screenplay), Stan Lee (Marvel comic book), Jack Kirby (Marvel comic book)",
    "elenco": "Miles Teller, Michael B. Jordan, Kate Mara, Jamie Bell",
    "sinopse": "FANTASTIC FOUR, uma recriação contemporânea da equipe de super-heróis da Marvel, é centrada em quatro jovens que se teleportam para um universo alternativo e perigoso, que altera sua forma física de maneira chocante. Suas vidas irrevogavelmente suspensas, a equipe deve aprender a aproveitar suas novas habilidades assustadoras e trabalhar juntos para salvar a Terra de um ex-amigo que se tornou inimigo.",
    "idioma": "Inglês",
    "pais": "EUA, Alemanha, Reino Unido",
    "premiacoes": "8 vitórias e 4 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTk0OTMyMDA0OF5BMl5BanBnXkFtZTgwMzY5NTkzNTE@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "4.3/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "9%"
      },
      {
        "Source": "Metacritic",
        "Value": "27/100"
      }
    ],
    "Metascore": 27,
    "imdbRating": 4.3,
    "imdbVotes": 135144,
    "Type": "movie",
    "DVD": "15 Dec 2015",
    "BoxOffice": 49179296,
    "Production": "20th Century Fox",
    "Website": "http://www.FantasticFourMovie.com",
    "Response": "True"
  },
  {
    "id": 1133985,
    "titulo": "Green Lantern",
    "ano": 2011,
    "classificacao": "PG-13",
    "lancamento": "17 de junho de 2011",
    "duracao": "114 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Martin Campbell",
    "roteirista": "Greg Berlanti (screenplay), Michael Green (screenplay), Marc Guggenheim (screenplay), Michael Goldenberg (screenplay), Greg Berlanti (screen story), Michael Green (screen story), Marc Guggenheim (screen story)",
    "elenco": "Ryan Reynolds, Blake Lively, Peter Sarsgaard, Mark Strong",
    "sinopse": "Em um universo misterioso, a Tropa dos Lanternas Verdes, uma força de defesa de elite de paz e justiça, existe há séculos. O imprudente piloto de testes Hal Jordan adquire poderes sobre-humanos quando é escolhido pelo Anel, a fonte de poder alimentada pela força de vontade. Relutante no início, ele aceita o desafio após a morte de Abin Sur, o melhor Lanterna Verde. Colocando suas dúvidas afora de lado, e estimulado por seu senso de dever e amor por sua bela e intelectualmente igual colega, Carol Ferris, ele logo é chamado para defender a humanidade de Parallax, um ser maligno e poderoso que se alimenta de medo. Hal Jordan é a última chance do universo, já que muitos Lanternas Verdes foram mortos e o Corpo está enfraquecido. E ele pode ser o Lanterna Verde certo para o dever de manter o mundo a salvo do perigo.",
    "idioma": "Inglês",
    "pais": "EUA",
    "premiacoes": "3 vitórias e 6 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTMyMTg3OTM5Ml5BMl5BanBnXkFtZTcwNzczMjEyNQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "5.5/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "26%"
      },
      {
        "Source": "Metacritic",
        "Value": "39/100"
      }
    ],
    "Metascore": 39,
    "imdbRating": 5.5,
    "imdbVotes": 246737,
    "Type": "movie",
    "DVD": "14 Oct 2011",
    "BoxOffice": 116600000,
    "Production": "Warner Bros. Pictures",
    "Website": "http://greenlanternmovie.warnerbros.com/",
    "Response": "True"
  },
  {
    "id": 359013,
    "titulo": "Blade: Trinity",
    "ano": 2004,
    "classificacao": "R",
    "lancamento": "08 de dezembro de 2004",
    "duracao": "113 min",
    "genero": "Ação, Aventura, Terror, Ficção Científica, Suspense",
    "diretor": "David S. Goyer",
    "roteirista": "David S. Goyer, Marv Wolfman (character), Gene Colan (character)",
    "elenco": "Wesley Snipes, Kris Kristofferson, Dominic Purcell, Jessica Biel",
    "sinopse": "Blade se encontra sozinho cercado por inimigos, lutando em uma batalha contra a nação vampira e agora com humanos. Ele une forças com um grupo de caçadores de vampiros que se chamam os Nightstalkers. A nação vampira desperta o rei dos vampiros Drácula de seu sono com intenções de usar seu sangue primitivo para se tornar caminhantes do dia. Do outro lado está Blade e sua equipe manifestando um vírus que poderia acabar com a raça de vampiros de uma vez por todas. No final, os dois lados irão colidir e apenas um sairá vitorioso, uma batalha entre o vampiro final que nunca conheceu a derrota, enfrentando o maior matador de vampiros.",
    "idioma": "Inglês, esperanto",
    "pais": "EUA",
    "premiacoes": "1 nomeação.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjE0Nzg2MzI3MF5BMl5BanBnXkFtZTYwMjExODQ3._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "5.9/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "25%"
      },
      {
        "Source": "Metacritic",
        "Value": "38/100"
      }
    ],
    "Metascore": 38,
    "imdbRating": 5.9,
    "imdbVotes": 150924,
    "Type": "movie",
    "DVD": "26 Apr 2005",
    "BoxOffice": 52400000,
    "Production": "New Line Cinema",
    "Website": "http://bladetrinity.com/",
    "Response": "True"
  },
  {
    "id": 1430132,
    "titulo": "The Wolverine",
    "ano": 2013,
    "classificacao": "PG-13",
    "lancamento": "26 de julho de 2013",
    "duracao": "126 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "James Mangold",
    "roteirista": "Mark Bomback (screenplay), Scott Frank (screenplay)",
    "elenco": "Hugh Jackman, Tao Okamoto, Rila Fukushima, Hiroyuki Sanada",
    "sinopse": "No Japão moderno, Wolverine está fora de sua profundidade em um mundo desconhecido como ele enfrenta seu maior inimigo em uma batalha de vida ou morte que vai deixá-lo mudado para sempre. Vulnerável pela primeira vez e levado a seus limites físicos e emocionais, ele confronta não apenas o aço samurai letal, mas também sua luta interior contra sua própria imortalidade, emergindo mais poderosa do que jamais o vimos antes.",
    "idioma": "Inglês, japonês",
    "pais": "EUA, Reino Unido, Austrália, Japão",
    "premiacoes": "2 vitórias e 11 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNzg1MDQxMTQ2OF5BMl5BanBnXkFtZTcwMTk3MjAzOQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.7/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "70%"
      },
      {
        "Source": "Metacritic",
        "Value": "61/100"
      }
    ],
    "Metascore": 61,
    "imdbRating": 6.7,
    "imdbVotes": 386934,
    "Type": "movie",
    "DVD": "03 Dec 2013",
    "BoxOffice": 132550960,
    "Production": "20th Century Fox",
    "Website": "http://www.thewolverinemovie.com/us/",
    "Response": "True"
  },
  {
    "id": 286716,
    "titulo": "Hulk",
    "ano": 2003,
    "classificacao": "PG-13",
    "lancamento": "20 de junho de 2003",
    "duracao": "138 min",
    "genero": "Ação, Ficção Científica",
    "diretor": "Ang Lee",
    "roteirista": "Stan Lee (Marvel comic book character), Jack Kirby (Marvel comic book character), James Schamus (story), John Turman (screenplay), Michael France (screenplay), James Schamus (screenplay)",
    "elenco": "Eric Bana, Jennifer Connelly, Sam Elliott, Josh Lucas",
    "sinopse": "Bruce Banner, um cientista brilhante com um passado nebuloso sobre sua família, está envolvido em um acidente em seu laboratório, fazendo com que ele seja exposto à radiação gama e Nanomeds (Uma pequena forma de vida que supostamente cura feridas, mas matou tudo com que eles fizeram contato). Confuso e curioso sobre sua sobrevivência, Banner descobre que desde o acidente, sempre que fica zangado, ele se transforma em um gigante monstro verde destruindo tudo à vista em um ato de fúria. O misterioso passado de Bruce e a resposta de por que a radiação teve esse efeito se torna revelada a ele enquanto seu pai de nascimento, David Banner, intervém com esperanças de continuar experimentando nele.",
    "idioma": "Inglês espanhol",
    "pais": "EUA",
    "premiacoes": "3 vitórias e 14 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNjcxMzZhZTEtMmEwYi00NDJmLWE5ZmUtOWJiMzRmMzEzMTY3L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "5.7/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "61%"
      },
      {
        "Source": "Metacritic",
        "Value": "54/100"
      }
    ],
    "Metascore": 54,
    "imdbRating": 5.7,
    "imdbVotes": 234058,
    "Type": "movie",
    "DVD": "28 Oct 2003",
    "BoxOffice": 132122995,
    "Production": "Universal Pictures",
    "Website": "http://www.thehulk.com/",
    "Response": "True"
  },
  {
    "id": 259324,
    "titulo": "Ghost Rider",
    "ano": 2007,
    "classificacao": "PG-13",
    "lancamento": "16 de fevereiro de 2007",
    "duracao": "114 min",
    "genero": "Ação, Aventura, Fantasia, Terror, Thriller",
    "diretor": "Mark Steven Johnson",
    "roteirista": "Mark Steven Johnson (screenplay), Mark Steven Johnson (screen story)",
    "elenco": "Matt Long, Raquel Alessi, Brett Cullen, Peter Fonda",
    "sinopse": "Quando o motociclista Johnny Blaze descobre que seu pai Barton Blaze tem um câncer terminal, ele aceita um pacto com o Mefistófeles, dando sua alma pela saúde de seu amado pai. Mas o diabo o engana e Barton morre em um acidente de moto durante uma exposição. Johnny deixa o carnaval, sua cidade, seus amigos e sua namorada Roxanne. Anos mais tarde, Johnny Blaze se torna um famoso motociclista, que arrisca sua vida em seus shows, e ele encontra novamente Roxanne, agora uma repórter de TV. No entanto, Mephistopheles propõe Johnny para liberar seu contrato, se ele se tornar o \"Ghost Rider\" e derrotar seu filho maléfico Blackheart, que quer possuir mil almas do mal e transformar o inferno na terra.",
    "idioma": "Inglês",
    "pais": "EUA, Austrália",
    "premiacoes": "1 vitória e 9 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMzIyNDE5ODI1OV5BMl5BanBnXkFtZTcwNTIyNDE0MQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "5.2/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "27%"
      },
      {
        "Source": "Metacritic",
        "Value": "35/100"
      }
    ],
    "Metascore": 35,
    "imdbRating": 5.2,
    "imdbVotes": 204101,
    "Type": "movie",
    "DVD": "12 Jun 2007",
    "BoxOffice": 115802596,
    "Production": "Sony Pictures",
    "Website": "http://www.sonypictures.com/movies/ghostrider/index.html",
    "Response": "True"
  },
  {
    "id": 450314,
    "titulo": "Punisher: War Zone",
    "ano": 2008,
    "classificacao": "R",
    "lancamento": "05 de dezembro de 2008",
    "duracao": "103 min",
    "genero": "Ação, Aventura, Crime, Drama, Thriller",
    "diretor": "Lexi Alexander",
    "roteirista": "Nick Santora, Art Marcum, Matt Holloway",
    "elenco": "Ray Stevenson, Dominic West, Doug Hutchison, Colin Salmon",
    "sinopse": "Frank Castle, o ex-militar cuja família foi morta por criminosos, que se tornou um vigilante conhecido como o Justiceiro, vai atrás de toda a família da turba e recebe todos, exceto o agente de segurança Billy Russoti. Ele rastreia Russoti para baixo e o persegue em um tanque que é usado para esmagar garrafas. Frank liga o britador esperando que ele cuidasse dele, mas isso não acontece. Ele sobrevive, mas sofre lesões muito graves que, mesmo com a cirurgia plástica, seu rosto parece um quebra-cabeça. Então ele decide adotar o nome Jigsaw. Frank, que matou um dos Russoti que é desconhecido para ele é um Fed disfarçado, decide fazer as malas. Mas quando ele descobre que Russoti não morreu e está procurando o dinheiro que ele confiou ao Fed e irá procurá-lo em sua casa. casa que significa que sua família está em perigo. Então Frank tenta salvá-los. Mas Russoti quer se vingar de Frank, então ele quebra seu irmão tão louco que está comprometido com um asilo para lidar com ele. E também outro Fed que é amigo do homem que Frank matou também o quer.",
    "idioma": "Inglês",
    "pais": "EUA, Canadá, Alemanha",
    "premiacoes": "1 vitória e 1 nomeação.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNDc1ODUyMzUtNDBkZS00YThiLWEzMjEtMGRlOTIzZWU2ZDdmXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.0/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "28%"
      },
      {
        "Source": "Metacritic",
        "Value": "30/100"
      }
    ],
    "Metascore": 30,
    "imdbRating": 6.0,
    "imdbVotes": 55384,
    "Type": "movie",
    "DVD": "17 Mar 2009",
    "BoxOffice": 7948159,
    "Production": "Lionsgate",
    "Website": "http://www.punisherwarzonemovie.com/",
    "Response": "True"
  },
  {
    "id": 120667,
    "titulo": "Fantastic Four",
    "ano": 2005,
    "classificacao": "PG-13",
    "lancamento": "08 de julho de 2005",
    "duracao": "106 min",
    "genero": "Ação, aventura, fantasia",
    "diretor": "Tim Story",
    "roteirista": "Mark Frost, Michael France, Stan Lee (Marvel comic book), Jack Kirby (Marvel comic book)",
    "elenco": "Ioan Gruffudd, Jessica Alba, Chris Evans, Michael Chiklis",
    "sinopse": "Reed Richards, um cientista brilhante, mas tímido e falido, está convencido de que a evolução pode ser desencadeada por nuvens de energia cósmica, e calculou que a Terra vai passar por uma dessas nuvens em breve. Juntamente com seu amigo e parceiro, o musculoso astronauta Ben Grimm, Reed convence seu conceituado colega do MIT, Dr. Victor Von Doom, agora CEO de sua própria empresa, a permitir o acesso à sua estação espacial privada. Von Doom concorda em troca de controle sobre o experimento e a maioria dos lucros de quaisquer benefícios que ele traz. Assim, ele traz a bordo Susan Storm, sua tímida, embora assertiva pesquisadora-chefe de genética e ex-amante de Reed, com quem ela teve um rompimento desagradável, e seu irmão Johnny, o piloto rebelde e impetuoso. Os astronautas chegam em casa intactos; no entanto, em pouco tempo eles começam a sofrer mutações, desenvolvendo poderes estranhos e surpreendentes como resultado de sua exposição à nuvem! Reed é capaz de se esticar como borracha; Sue pode se tornar invisível e criar campos de força, especialmente quando está com raiva; Johnny pode produzir fogo em temperaturas de supernova e é capaz de voar; e Ben é transformado em \"The Thing\", uma grande criatura parecida com uma rocha com super força. Depois de Ben, meditando sobre sua situação na ponte do Brooklyn, inadvertidamente causa um grande acúmulo de tráfego enquanto tenta impedir um homem prestes a cometer suicídio, os quatro conseguem usar seus poderes para evitar qualquer perda de vida e resgatar um caminhão de bombeiros. e sua tripulação de cair da ponte em uma explosão resultante. A mídia chama o time de 'Quarteto Fantástico' e, embora Johnny abraça ansiosamente seus poderes e nova vida, Ben - o mais desfigurado - sofre particularmente com sua transformação; sua desfiguração fez com que sua noiva o abandonasse e viu-o evitado e temido por grande parte de Nova York. Culpando-se, Reed promete devolver Ben à sua forma humana, e ele, Sue e Ben trabalham em uma cura, construindo uma câmara de cura no prédio de alta tecnologia do Bed de Reed, transformado em laboratório. Durante esse tempo, Reed e Susan começam a se aproximar mais uma vez, e Susan admite que não está interessada em Victor, mas terminou seu relacionamento porque Reed temia se comprometer, pensando apenas em termos de variáveis. Desconhecido para os outros, no entanto, o corpo de Victor também está em mutação; ele está se transformando em metal orgânico capaz de absorver e manipular a energia elétrica conhecida como Doctor Doom. Como resultado da expedição desastrosa, sua empresa está indo à falência e ele está perdendo estatura pública; culpando Reed por seus infortúnios, Victor jura vingança. E Reed, Ben, Susan e Johnny devem derrotar Victor Von Doom e frustrar seus planos malignos de uma vez por todas.",
    "idioma": "Inglês",
    "pais": "EUA, Alemanha",
    "premiacoes": "3 vitórias e 12 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNWU1ZjFjMTctYjA5ZC00YTBkLTkzZjUtZWEyMjgxY2MxYWM4XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "5.7/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "27%"
      },
      {
        "Source": "Metacritic",
        "Value": "40/100"
      }
    ],
    "Metascore": 40,
    "imdbRating": 5.7,
    "imdbVotes": 290149,
    "Type": "movie",
    "DVD": "06 Dec 2005",
    "BoxOffice": 154485963,
    "Production": "20th Century Fox",
    "Website": "http://www.fantasticfourmovie.com/",
    "Response": "True"
  },
  {
    "id": 81573,
    "titulo": "Superman II",
    "ano": 1980,
    "classificacao": "PG",
    "lancamento": "19 de junho de 1981",
    "duracao": "127 min",
    "genero": "Ação, Aventura, Ficção Científica",
    "diretor": "Richard Lester, Richard Donner",
    "roteirista": "Jerry Siegel (character created by: Superman), Joe Shuster (character created by: Superman), Mario Puzo (story by), Mario Puzo (screenplay by), David Newman (screenplay by), Leslie Newman (screenplay by)",
    "elenco": "Gene Hackman, Christopher Reeve, Ned Beatty, Jackie Cooper",
    "sinopse": "Continuando onde \"Superman: O Filme\" parou, três criminosos, General Zod (Terence Stamp), Ursa (Sarah Douglas) e Non (Jack O'Halloran) do planeta Krypton são libertados da Zona Fantasma por um nuclear explosão no espaço. Eles descem sobre a Terra onde poderiam finalmente governar. Enquanto isso, o super-homem está apaixonado por Lois Lane (Margot Kidder), que descobre quem ele realmente é. Lex Luthor (Gene Hackman) escapa da prisão e está determinado a destruir o Super-Homem juntando forças com os três criminosos.",
    "idioma": "Inglês, francês, russo",
    "pais": "EUA, Reino Unido, Canadá",
    "premiacoes": "3 vitórias e 6 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMWUzNDJjMDUtNGEzMi00YThlLTlkMmEtNjVlOGQwOWM3MjM3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "6.8/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "85%"
      },
      {
        "Source": "Metacritic",
        "Value": "87/100"
      }
    ],
    "Metascore": 87,
    "imdbRating": 6.8,
    "imdbVotes": 87920,
    "Type": "movie",
    "DVD": "01 May 2001",
    "BoxOffice": "N/D",
    "Production": "Warner Bros. Pictures",
    "Website": "N/D",
    "Response": "True"
  },
  {
    "id": 327554,
    "titulo": "Catwoman",
    "ano": 2004,
    "classificacao": "PG-13",
    "lancamento": "23 de julho de 2004",
    "duracao": "104 min",
    "genero": "Ação, Aventura, Crime, Drama, Fantasia, Romance",
    "diretor": "Pitof",
    "roteirista": "Bob Kane (characters), Theresa Rebeck (story), John Brancato (story), Michael Ferris (story), John Brancato (screenplay), Michael Ferris (screenplay), John Rogers (screenplay)",
    "elenco": "Halle Berry, Benjamin Bratt, Sharon Stone, Lambert Wilson",
    "sinopse": "Catwoman é a história da tímida e sensível artista Patience Philips, uma mulher que parece não conseguir parar de se desculpar por sua própria existência. Ela trabalha como designer gráfico para a Hedare Beauty, uma gigantesca empresa de cosméticos prestes a lançar um revolucionário produto antienvelhecimento. Quando Paciência inadvertidamente acontece em um segredo obscuro que seu empregador está escondendo, ela se encontra no meio de uma conspiração corporativa. O que acontece depois muda Paciência para sempre. Em uma reviravolta mística do destino, ela é transformada em uma mulher com a força, a velocidade, a agilidade e os sentidos ultra-aguçados de um gato. Com sua nova coragem e intuição felina, Patience se torna Catwoman, uma criatura elegante e furtiva que se equilibra na linha tênue entre o bem e o mal. Como qualquer gato selvagem, ela é perigosa, indescritível e indomável. Suas aventuras são complicadas por um florescente relacionamento com Tom Lone, um policial que se apaixona por Patience, mas não consegue abalar sua fascinação com a misteriosa Catwoman, que parece ser responsável por uma série de crimes que assolam a cidade.",
    "idioma": "Inglês espanhol",
    "pais": "EUA",
    "premiacoes": "13 vitórias e 8 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMjA4MzM0NDAzOF5BMl5BanBnXkFtZTcwMDY3MDYyMQ@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "3.3/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "9%"
      },
      {
        "Source": "Metacritic",
        "Value": "27/100"
      }
    ],
    "Metascore": 27,
    "imdbRating": 3.3,
    "imdbVotes": 98556,
    "Type": "movie",
    "DVD": "18 Jan 2005",
    "BoxOffice": 40200000,
    "Production": "Warner Bros. Pictures",
    "Website": "http://catwoman.warnerbros.com/",
    "Response": "True"
  },
  {
    "id": 357277,
    "titulo": "Elektra",
    "ano": 2005,
    "classificacao": "PG-13",
    "lancamento": "14 de janeiro de 2005",
    "duracao": "97 min",
    "genero": "Ação, Aventura, Crime, Fantasia, Suspense",
    "diretor": "Rob Bowman",
    "roteirista": "Mark Steven Johnson (motion picture characters), Frank Miller (comic book characters), Zak Penn, Stuart Zicherman, M. Raven Metzner",
    "elenco": "Jennifer Garner, Goran Visnjic, Kirsten Zien, Will Yun Lee",
    "sinopse": "Na batalha final entre o bem e o mal está um guerreiro que faz a escolha que inclina o equilíbrio. Uma heroína de ação forte, misteriosa e mortalmente sexy - uma síntese letal de graça e poder. Não muito tempo depois de se recuperar de feridas aparentemente mortais, Elektra cortou todos os laços com o mundo, vivendo apenas para sua próxima missão. Mas em uma virada inesperada de eventos, ela é forçada a tomar uma decisão que pode levar sua vida em uma nova direção - ou destruí-la. Os principais jogadores da jornada de Elektra são Stick, um mestre cego de artes marciais responsável pela \"ressurreição\" de Elektra, e Mark Miller e Abby Miller, pai e filha fugindo de The Hand, um poderoso sindicato cujos membros praticam a versão sombria do marcial. arte Kimagure.",
    "idioma": "Inglês, japonês",
    "pais": "Canadá, EUA",
    "premiacoes": "2 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTI3MTUwNzM5MV5BMl5BanBnXkFtZTcwNzczMDIzMw@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "4.7/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "10%"
      },
      {
        "Source": "Metacritic",
        "Value": "34/100"
      }
    ],
    "Metascore": 34,
    "imdbRating": 4.7,
    "imdbVotes": 79818,
    "Type": "movie",
    "DVD": "05 Apr 2005",
    "BoxOffice": 24100000,
    "Production": "20th Century Fox",
    "Website": "http://www.elektramovie.com/",
    "Response": "True"
  },
  {
    "id": 88206,
    "titulo": "Supergirl",
    "ano": 1984,
    "classificacao": "PG",
    "lancamento": "21 de novembro de 1984",
    "duracao": "150 min",
    "genero": "Ação, aventura, fantasia",
    "diretor": "Jeannot Szwarc",
    "roteirista": "David Odell (screenplay)",
    "elenco": "Faye Dunaway, Helen Slater, Peter O'Toole, Hart Bochner",
    "sinopse": "Depois que uma fonte de energia para a comunidade de sobreviventes de Krypton é acidentalmente levada à Terra, Kara-El, primo de Super-homem e sobrinha de Jor-El, decide ir à Terra encontrá-la e trazê-la de volta. Após a sua chegada, ela se torna apenas uma poderosa e Super como sua prima, mas encontra batalhas perigosas e obstáculos inesperados quando uma mulher espirituosa que pratica rituais do oculto toma a fonte de energia para si mesma e a usa para causar destruição e tentativa de zênite. status.",
    "idioma": "Inglês",
    "pais": "Reino Unido, EUA",
    "premiacoes": "3 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BNjE1ZDYyNDAtNzQ1My00YjM0LWI5ZWItNTBmMDg2Nzg2ZmNmXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "4.4/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "10%"
      },
      {
        "Source": "Metacritic",
        "Value": "42/100"
      }
    ],
    "Metascore": 42,
    "imdbRating": 4.4,
    "imdbVotes": 16224,
    "Type": "movie",
    "DVD": "08 Aug 2000",
    "BoxOffice": "N/D",
    "Production": "TriStar Pictures",
    "Website": "N/D",
    "Response": "True"
  },
  {
    "id": 1071875,
    "titulo": "Ghost Rider: Spirit of Vengeance",
    "ano": 2011,
    "classificacao": "PG-13",
    "lancamento": "17 de fevereiro de 2012",
    "duracao": "96 min",
    "genero": "Ação, Fantasia, Thriller",
    "diretor": "Mark Neveldine, Brian Taylor",
    "roteirista": "Scott M. Gimple (screenplay), Seth Hoffman (screenplay), David S. Goyer (screenplay), David S. Goyer (story)",
    "elenco": "Nicolas Cage, Violante Placido, Ciarán Hinds, Idris Elba",
    "sinopse": "Johnny Blaze, um homem que fez um acordo com o Diabo que se chamava Mefistófeles na época (agora Roarke), está fugindo tentando se certificar de que ninguém é prejudicado por seu alter ego, The Ghost Rider. Ele é abordado por um Monk chamado Moreau que lhe diz que ele pode ajudá-lo a se libertar do Cavaleiro, mas primeiro, ele precisa da ajuda de Johnny para proteger um garoto, a quem Roarke tem planos, para ajudá-lo a assumir a forma humana.",
    "idioma": "Inglês, romeno",
    "pais": "EUA, Emirados Árabes Unidos",
    "premiacoes": "4 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTkwNDM5MDEzOF5BMl5BanBnXkFtZTcwNDEyNTUxNw@@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "4.3/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "17%"
      },
      {
        "Source": "Metacritic",
        "Value": "34/100"
      }
    ],
    "Metascore": 34,
    "imdbRating": 4.3,
    "imdbVotes": 100432,
    "Type": "movie",
    "DVD": "12 Jun 2012",
    "BoxOffice": 51774002,
    "Production": "Sony Pictures",
    "Website": "http://www.thespiritofvengeance.com/",
    "Response": "True"
  },
  {
    "id": 486576,
    "titulo": "Fantastic 4: Rise of the Silver Surfer",
    "ano": 2007,
    "classificacao": "PG",
    "lancamento": "15 de junho de 2007",
    "duracao": "92 min",
    "genero": "Ação, aventura, fantasia",
    "diretor": "Tim Story",
    "roteirista": "Don Payne (screenplay), Mark Frost (screenplay), John Turman (story), Mark Frost (story), Stan Lee (characters), Jack Kirby (characters)",
    "elenco": "Ioan Gruffudd, Jessica Alba, Chris Evans, Michael Chiklis",
    "sinopse": "Tudo parece estar indo muito bem para o Quarteto Fantástico. Reed e Sue finalmente vão se casar, e as coisas não poderiam parecer melhores. No entanto, quando o misterioso Surfista Prateado quebra as coisas, eles descobrem que terão que lidar com um velho inimigo, e o poderoso planeta comendo Galactus.",
    "idioma": "Inglês, japonês, chinês, árabe",
    "pais": "EUA, Alemanha, Reino Unido",
    "premiacoes": "2 vitórias e 14 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BODA4YTc5N2QtNzQyYS00ZDUzLWI3M2UtZWI2OWVhOGZlN2MxXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "5.6/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "37%"
      },
      {
        "Source": "Metacritic",
        "Value": "45/100"
      }
    ],
    "Metascore": 45,
    "imdbRating": 5.6,
    "imdbVotes": 232808,
    "Type": "movie",
    "DVD": "02 Oct 2007",
    "BoxOffice": 131766943,
    "Production": "20th Century Fox",
    "Website": "http://www.fantasticfourmovie.com/",
    "Response": "True"
  },
  {
    "id": 86393,
    "titulo": "Superman III",
    "ano": 1983,
    "classificacao": "PG",
    "lancamento": "17 de junho de 1983",
    "duracao": "125 min",
    "genero": "Ação, Comédia, Ficção Científica",
    "diretor": "Richard Lester",
    "roteirista": "Jerry Siegel (character created by: Superman), Joe Shuster (character created by: Superman), David Newman (screenplay by), Leslie Newman (screenplay by)",
    "elenco": "Christopher Reeve, Richard Pryor, Jackie Cooper, Marc McClure",
    "sinopse": "O rico empresário Ross Webster (Robert Vaughn) descobre os talentos ocultos de Gus Gorman (Richard Pryor), um gênio da computação travesso. Ross decide abusar de seus talentos, de forma a ajudar Webster em seus planos de controle econômico. Quando o homem de aço interferir, algo deve ser feito com relação a Supes. Quando a criptonita sintética de Gus não consegue matar o Super-homem, isso o transforma em uma encarnação maligna de seu antigo eu. O Kryptonite tar-atado coloca o homem contra si mesmo, estabelecendo a batalha Clark vs. Superman.",
    "idioma": "Inglês, italiano, espanhol",
    "pais": "Reino Unido, EUA",
    "premiacoes": "2 vitórias e 7 nomeações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMzI3ZDllMTctNmI2Mi00OGQ4LTk2ZTQtYTJhMjA5ZGI2YmRkXkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "4.9/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "24%"
      },
      {
        "Source": "Metacritic",
        "Value": "44/100"
      }
    ],
    "Metascore": 44,
    "imdbRating": 4.9,
    "imdbVotes": 57628,
    "Type": "movie",
    "DVD": "01 May 2001",
    "BoxOffice": "N/D",
    "Production": "WARNER BROTHERS PICTURES",
    "Website": "N/D",
    "Response": "True"
  },
  {
    "id": 94074,
    "titulo": "Superman IV: The Quest for Peace",
    "ano": 1987,
    "classificacao": "PG",
    "lancamento": "24 de julho de 1987",
    "duracao": "90 min",
    "genero": "Ação, Aventura, Família",
    "diretor": "Sidney J. Furie",
    "roteirista": "Jerry Siegel (character created by: Superman), Joe Shuster (character created by: Superman), Christopher Reeve (story by), Lawrence Konner (story by), Mark Rosenthal (story by), Lawrence Konner (screenplay by), Mark Rosenthal (screenplay by)",
    "elenco": "Christopher Reeve, Gene Hackman, Jackie Cooper, Marc McClure",
    "sinopse": "Superman faz muito em sua mais nova aventura. O arquivista Lex Luthor, determinado a tornar o mundo seguro para os comerciantes de armas nucleares, cria um novo ser para desafiar o Homem de Aço: o Homem Nuclear carregado de radiação. Os dois superpoderosos inimigos colidem em uma tela explosiva extranvaganza que faz o Superman salvar a Estátua da Liberdade, repelir uma erupção vulcânica do Monte Etna, reconstruir a Grande Muralha da China demolida e realizar muitos outros feitos espectrativos.",
    "idioma": "Inglês, russo, francês, italiano",
    "pais": "Reino Unido, EUA",
    "premiacoes": "2 vitórias e 6 indicações.",
    "poster": "https://m.media-amazon.com/images/M/MV5BMmIwZWY1YTYtNDlhOS00NDRmLWI4MzItNjk2NDc1N2NhYzNlXkEyXkFqcGdeQXVyNTUyMzE4Mzg@._V1_SX300.jpg",
    "Ratings": [
      {
        "Source": "Internet Movie Database",
        "Value": "3.7/10"
      },
      {
        "Source": "Rotten Tomatoes",
        "Value": "12%"
      },
      {
        "Source": "Metacritic",
        "Value": "24/100"
      }
    ],
    "Metascore": 24,
    "imdbRating": 3.7,
    "imdbVotes": 37838,
    "Type": "movie",
    "DVD": "01 May 2001",
    "BoxOffice": "N/D",
    "Production": "Warner Home Video",
    "Website": "N/D",
    "Response": "True"
  }
]
// #endregion

const localDb = localStorage.getItem('db')

const db = localDb ? JSON.parse(localDb) : fakeDb

localStorage.setItem('db', JSON.stringify(db))

const database = new Database(db)

function Database(items) {
  this.adicionar = adicionar
  this.obter = obter
  this.obterTodos = obterTodos
  this.atualizar = atualizar
  this.remover = remover

  function adicionar(item) {
    const novoItem = { ...item, id: gerarNovoId() }
    items.push(novoItem)
    localStorage.setItem('db', JSON.stringify(items))
    return novoItem
  }

  function obter(id) {
    return items.find(item => item.id === id)
  }

  function obterTodos() {
    return $.extend(true, items, [])
  }

  function atualizar(id, options) {
    const item = obter(id)
    if (item) {
      const novoItem = {
        ...item,
        ...options,
        id: item.id
      }

      const itemIndex = items.findIndex(i => i.id === item.id)
      items.splice(itemIndex, 1, novoItem)
      localStorage.setItem('db', JSON.stringify(items))
    }
  }

  function remover(id) {
    const index = items.findIndex(i => i.id === id)
    if (index > -1) {
      items.splice(index, 1)
      localStorage.setItem('db', JSON.stringify(items))
    }
  }

  function gerarNovoId() {
    const maiorId = items.reduce((prev, next) => {
      return Number(prev.id) > Number(next.id) ? prev : next
    }, { id: 0 })
    return Number(maiorId.id) + 1
  }

}

