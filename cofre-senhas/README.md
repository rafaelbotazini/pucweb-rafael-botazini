# Cofre de sehas
## Trabalho Interdisciplinar de Software I

[![js-semistandard-style](https://img.shields.io/badge/code%20style-semistandard-brightgreen.svg?style=flat-square)](https://github.com/Flet/semistandard)

Este programa é um repositório de senhas personalizável com categorias.

* [Como configurar o ambiente de desenvolvimento](AmbienteDesenvolvimento.md)