var db = firebase.firestore();
var user = JSON.parse(localStorage.getItem('user') || '{}');

if (!user.uid) {
  window.location.href = 'login.html';
}

db.collection('usuarios')
  .doc(user.uid)
  .get()
  .then(function (doc) {
    if (!doc.exists) {
      db.collection('usuarios')
        .doc(user.uid)
        .set(user)
        .then(initApp);
    } else {
      initApp();
    }
  });

// Referencias das coleções/tabelas no banco
var senhas = db.collection('senhas');
var categorias = db.collection('categorias');

function initApp() {
  $(onready);
}

function onready() {
  // Populando categorias do usuário
  categorias.where('usuario', '==', user.uid).get()
    .then(function (docs) {
      docs.forEach(renderCategoria);

      if (docs.empty) {
        $('.placeholder').show();
      }

      // Populando as senhas
      senhas.where('usuario', '==', user.uid).get()
        .then(function (docs) {
          docs.forEach(renderChave);
          $('.loading').hide();
          $('main').show();
        });
    });

  // #region EVENTOS
  $('#btn-criar-categoria').click(criarCategoria);
  $('#modalCriarChave .btn-confirm').click(criarChave);

  $('#modalEditarCategoria .btn-confirm').click(function () {
    editarCategoria($(this).data('categoria'));
  });

  $('#btn-salvar-chave').click(function () {
     editarChave($(this).data('chave'));
  });

  $('#btn-editar-chave').click(function () {
    $('#modalExibirChave').modal('hide');
    exibirModalEditarChave($(this).data('chave'));
  });

  $('#btn-excluir-chave').click(function () {
    excluirChave($(this).data('chave'));
  });

  $('.revelar-senha').click(revelarEsconderSenha);

  // limpar campos ao fechar os modals
  $('#modalExibirChave').on('hidden.bs.modal', function () {
    var $modal = $('#modalExibirChave');
    $modal.find('.nome-chave').html('');
    $modal.find('[name="login-chave"]').val('');
    $modal.find('[name="senha-chave"]').val('');
    esconderSenha();
  });

  $('#modalCriarChave').on('hidden.bs.modal', function () {
    var $modal = $('#modalCriarChave');
    $modal.find('#nome-chave').val('');
    $modal.find('#login-chave').val('');
    $modal.find('#senha-chave').val('');
    esconderSenha();
  });

  // #endregion
}

function criarCategoria() {
  var nome = $('#nome-categoria').val();

  if (nome) {
    var categoria = {
      nome: nome,
      usuario: user.uid
    };

    // Adicionando categoria no banco
    categorias.add(categoria).then(function (docRef) {
      // Renderizando na tela
      docRef.get().then(renderCategoria)
      $('#modalCriarCategoria').modal('hide');
    });

  } else {
    alert('Defina o nome da nova categoria');
  }
}

function renderCategoria(doc) {
  if (doc.exists) {
    $('.placeholder').hide();

    var categoria = doc.data();
    var $card = $('<div />', {
      class: 'card mb-2',
      id: doc.id
    });

    var $cardHeader = $('<div />', {
      class: 'card-header',
      html: getCategoriaHeader(doc.id, categoria.nome)
    });

    var $tableSenhas = $('<table/>', {
      class: 'table',
      html: '<tbody></tbody>'
    });

    var $cardBody = $('<div />', {
      class: 'collapse',
      id: 'collapse-' + doc.id,
    });

    var $btnAdicionarSenha = $('<button />', {
      type: 'button',
      class: 'btn btn-block btn-light',
      html: 'Adicionar senha',
    });

    $btnAdicionarSenha
      .click(function () {
        exibirModalAdicionarChave(
          $.extend(categoria, { id: doc.id })
        );
      });

    $cardBody.append(
      $tableSenhas,
      $btnAdicionarSenha
    );

    $card
      .append($cardHeader, $cardBody)
      .appendTo('.senhas');
  }
}

function getCategoriaHeader(id, nome) {
  var $header = $(
    '<div data-toggle="collapse"' +
      ' data-target="#collapse-' + id + '"' +
      ' class="d-flex">' +
        '<span class="nome-categoria">' + nome + '</span>' +
      '</div>'
  );

  var $btnEditarCategoria = $('<i class="fa fa-edit mr-2 text-info"></i>');
  var $btnRemoverCategoria = $('<i class="fa fa-trash text-danger"></i>');

  $btnRemoverCategoria.click(function () {
    removerCategoria(id);
  });

  $btnEditarCategoria.click(function () {
    exibirModalEditarCategoria(id);
  });

  $('<div class="ml-auto">')
    .append($btnEditarCategoria, $btnRemoverCategoria)
    .appendTo($header);

  return $header;
}

function removerCategoria(id) {
  var deletar = confirm('Tem certeza que deseja deletar essa categoria?');
  if (deletar) {
    categorias.doc(id).delete().then(function () {
      $('#' + id).remove();
    });
  }
}

function editarCategoria(id) {
  var $modal = $('#modalEditarCategoria');
  var nome = $modal.find('[name="nome-categoria"]').val();

  if (nome) {
    categorias.doc(id).update({
      nome: nome
    }).then(function () {
      $('#' + id + ' .nome-categoria').html(nome);
      notify('Atualizado com sucesso!');
      $modal.modal('hide');
    });
  } else {
    notify('Informe o nome da categoria.');
  }
}

function criarChave() {
  var $modal = $('#modalCriarChave');

  var keyring = $('#modalCriarChave .btn-confirm').data('keyring');
  var nome = $modal.find('#nome-chave').val();
  var login = $modal.find('#login-chave').val();
  var senha = $modal.find('#senha-chave').val();

  if (nome && login && senha) {
    senhas.add({
      usuario: user.uid,
      nome: nome,
      login: login,
      senha: senha,
      categorias: [keyring]
    }).then(function (doc) {
      $modal.modal('hide');
      doc.get().then(renderChave);
    });
  } else {
    notify('Todos os campos devem ser preenchidos.')
  }
}

function excluirChave(id) {
  var deletar = confirm('Tem certeza que deseja deletar esta chave?');

  if (deletar) {
    senhas.doc(id).get().then(function (doc) {
      var chave = doc.data();

      senhas.doc(id).delete().then(function () {
        chave.categorias.forEach(function (cat) {
          $('#' + cat).find('.' + id).remove();
          $('#modalExibirChave').modal('hide');
        });
      })
    });
  }
}

function editarChave(id) {
  var $modal = $('#modalEditarChave');
  var nome = $modal.find('[name="nome-chave"]').val();
  var login = $modal.find('[name="login-chave"]').val();
  var senha = $modal.find('[name="senha-chave"]').val();

  if (nome && login && senha) {
    senhas.doc(id).update({
      nome: nome,
      login: login,
      senha: senha
    }).then(function () {
      notify('Atualizado com sucesso!');
      $modal.modal('hide');
      exibirModalExibirChave(id);
    });
  } else {
    notify('Todos os campos devem ser preenchidos.')
  }
}

/**
 * Renderiza a chave na tela, em todas as categorias
 * que a chave estiver cadastrada
 * @param {*} doc doc do banco de dados
 */
function renderChave(doc) {
  if (doc.exists) {
    var chave = doc.data();
    var $rowSenha = $('<tr/>', {
      html: $('<td/>', {
        class: 'senha ' + doc.id,
        html: chave.nome
      })
    });

    $rowSenha
      .data('chave', doc.id)
      .click(function () {
        exibirModalExibirChave($(this).data('chave'));
      });

    chave.categorias.forEach(function (categoriaId) {
      $('#' + categoriaId)
        .find('tbody')
        .append($rowSenha);
    });
  }
}

/**
 * Mostra um modal para criação de uma categoria
 * @param {{nome:string, usuario:string, id:string}} categoria
 */
function exibirModalEditarCategoria(id) {
  categorias.doc(id).get().then(function (doc) {
    var categoria = doc.data();
    var $modal = $('#modalEditarCategoria');
  
    $modal
      .find('.modal-title')
      .html(categoria.nome + ' - Editar Categoria');

    $modal
      .find('.btn-confirm')
      .data('categoria', id);
  
    $modal
      .find('[name="nome-categoria"]')
      .val(categoria.nome);
  
    $modal.modal({
      backdrop: 'static',
      show: true
    });
  });
}

/**
 * Mostra um modal para criação de uma nova categoria
 * @param {{nome:string, usuario:string, id:string}} categoria
 */
function exibirModalAdicionarChave(categoria) {
  var $modal = $('#modalCriarChave');

  $modal
    .find('.btn-confirm')
    .data('keyring', categoria.id);

  $modal.find('.categoria-nome').html(categoria.nome);

  $modal.modal({
    backdrop: 'static',
    show: true
  });
}

/**
 * Mostra um modal para edição da chave
 * @param {string} id id da chave
 */
function exibirModalEditarChave(id) {
  senhas.doc(id).get().then(function (doc) {
    var chave = doc.data();
    var $modal = $('#modalEditarChave');
  
    $modal.find('.modal-title').html(chave.nome + ' - Editar dados');
    $modal.find('[name="nome-chave"]').val(chave.nome);
    $modal.find('[name="login-chave"]').val(chave.login);
    $modal.find('[name="senha-chave"]').val(chave.senha);
  
    $("#btn-salvar-chave").data('chave', id);
  
    $modal.find('.chave-nome').html(chave.nome);
  
    $modal.modal({
      backdrop: 'static',
      show: true
    });
  });
}

/**
 * Exibe um modal com os detalhes da chave
 * @param {{nome:string, login:string, senha:string}} chave 
 */
function exibirModalExibirChave(id) {
  senhas.doc(id).get().then(function (doc) {
    var chave = doc.data();
    var $modal = $('#modalExibirChave');

    $modal.find('.nome-chave').html(chave.nome);
    $modal.find('[name="login-chave"]').val(chave.login);
    $modal.find('[name="senha-chave"]').val(chave.senha);

    $modal.find('#btn-editar-chave').data('chave', id);
    $modal.find('#btn-excluir-chave').data('chave', id);
    $modal.modal('show');
  });
}

/**
 * Revela a senha se estiver escondida
 * ou esconde, se estiver à mostra
 */
function revelarEsconderSenha() {
  var type = $('#modalExibirChave [name="senha-chave"]')
    .attr('type');

  if (type === 'text') {
    esconderSenha();
  } else {
    revelarSenha();
  }
}

/**
 * Revela a senha do campo Senha,
 * mudando o atributo type para 'text'
 */
function revelarSenha() {
  $('.revelar-senha')
    .find('i.fa')
    .removeClass('fa-eye')
    .addClass('fa-eye-slash');

  $('.revelar-senha')
    .closest('.input-group')
    .find('input')
    .attr('type', 'text');
}

/**
 * Esconde a senha do campo Senha,
 * mudando o atributo type para 'password'
 */
function esconderSenha() {
  $('.revelar-senha')
    .find('i.fa')
    .addClass('fa-eye')
    .removeClass('fa-eye-slash');

  $('.revelar-senha')
    .closest('.input-group')
    .find('input')
    .attr('type', 'password');
}
