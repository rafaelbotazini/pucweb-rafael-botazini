$(function () {
  // firebase.auth().signOut();
  $('#signup-link').click(mostrarCriarSenha);
  $('#signin-link').click(mostrarTelaLogin);
  $('#btnCreate').click(criarUsuario);
  $('#btnSignIn').click(fazerLogin);

  $('section.signup [name="email"]').keypress(inputKeypress);
  $('section.signup [name="password"]').keypress(inputKeypress);
  $('section.signin [name="email"]').keypress(inputKeypress);
  $('section.signin [name="password"]').keypress(inputKeypress);

  $('section.signup').hide();
  $('section.signin').hide();
  esconderLoading();
});

// #region EVENTOS DO FIREBASE

// Usuário entrou ou saiu
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    window.localStorage.setItem('user', JSON.stringify(user));
    window.location.href = 'index.html';
  } else {
    mostrarTelaLogin();
  }
});

// #endregion EVENTOS DO FIREBASE

function inputKeypress(e) {
  if (e.key === 'Enter') {
    $(this).closest('form').find('button').click();
  }
}

function criarUsuario() {
  var email = $('section.signup [name="email"]').val();
  var password = $('section.signup [name="password"]').val();

  mostrarLoading();

  firebase.auth()
    .createUserWithEmailAndPassword(email, password)
    .then(esconderLoading)
    .catch(function (error) {
      notify(error.message);
    });
}

function fazerLogin() {
  var email = $('section.signin [name="email"]').val();
  var password = $('section.signin [name="password"]').val();

  firebase.auth()
    .signInWithEmailAndPassword(email, password)
    .catch(function (error) {
      notify(error.message);
    });
}

function mostrarCriarSenha() {
  $('section.signup').show();
  $('section.signin').hide();
}

function mostrarTelaLogin() {
  $('section.signup').hide();
  $('section.signin').show();
}

function mostrarLoading() {
  $('section.loading').show();
}

function esconderLoading() {
  $('section.loading').hide();
};