// Initialize Firebase
var config = {
  apiKey: 'AIzaSyCxTNk6AV5Uly1mIOKhTeV0QezSMDoBNqg',
  authDomain: 'cofre-de-senhas.firebaseapp.com',
  databaseURL: 'https://cofre-de-senhas.firebaseio.com',
  projectId: 'cofre-de-senhas',
  storageBucket: 'cofre-de-senhas.appspot.com',
  messagingSenderId: '181131887875'
};

var adminuid = 'ZuRFU9PCnIPFdvWqa2hRnsgRs493';

// #region CONFIGURAÇÃO DO FIREBASE
firebase.initializeApp(config);

firebase.auth()
  .setPersistence(firebase.auth.Auth.Persistence.SESSION);

firebase.firestore()
  .settings({ timestampsInSnapshots: true });

firebase.auth().languageCode = 'pt';

firebase.auth().onAuthStateChanged(function (user) {
  if (!user) {
    localStorage.removeItem('uid');
  } else {
    localStorage.setItem('uid', user.uid);
  }
});
// #endregion

$(function () {
  $('.btn-logout').click(function (e) {
    e.preventDefault();
    firebase.auth().signOut()
      .then(function () {
        window.location.href = 'login.html';
      });
  });
});

function notify(message, type) {
  window.alert(message);
}
